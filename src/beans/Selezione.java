package beans;

import java.io.Serializable;

public class Selezione implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idUtente;
	private int idImmagine;
	private boolean esito;
	/**
	 * @return the idUtente
	 */
	public int getIdUtente() {
		return idUtente;
	}
	/**
	 * @param idUtente the idUtente to set
	 */
	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}
	/**
	 * @return the idImmagine
	 */
	public int getIdImmagine() {
		return idImmagine;
	}
	/**
	 * @param idImmagine the idImmagine to set
	 */
	public void setIdImmagine(int idImmagine) {
		this.idImmagine = idImmagine;
	}
	/**
	 * @return the esito
	 */
	public boolean isEsito() {
		return esito;
	}
	/**
	 * @param esito the esito to set
	 */
	public void setEsito(boolean esito) {
		this.esito = esito;
	}
	
	
}
