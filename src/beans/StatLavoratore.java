package beans;

import java.io.Serializable;

public class StatLavoratore implements Serializable{
	private int idUtente=0;
	private int idCampagna=0;
	private int nImmAnnotate=0;
	private int nImmDaAnnotare=0;
	private int nImmApprovate=0;
	private int nImmRifiutate=0;
	private int nSelezioniFatte=0;
	private int nSelezioniDaFare=0;
	
	/**
	 * @return the idUtente
	 */
	public int getIdUtente() {
		return idUtente;
	}
	/**
	 * @param idUtente the idUtente to set
	 */
	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}
	/**
	 * @return the idCampagna
	 */
	public int getIdCampagna() {
		return idCampagna;
	}
	/**
	 * @param idCampagna the idCampagna to set
	 */
	public void setIdCampagna(int idCampagna) {
		this.idCampagna = idCampagna;
	}
	/**
	 * @return the nImmAnnotate
	 */
	public int getnImmAnnotate() {
		return nImmAnnotate;
	}
	/**
	 * @param nImmAnnotate the nImmAnnotate to set
	 */
	public void setnImmAnnotate(int nImmAnnotate) {
		this.nImmAnnotate = nImmAnnotate;
	}
	/**
	 * @return the nImmDaAnnotare
	 */
	public int getnImmDaAnnotare() {
		return nImmDaAnnotare;
	}
	/**
	 * @param nImmDaAnnotare the nImmDaAnnotare to set
	 */
	public void setnImmDaAnnotare(int nImmDaAnnotare) {
		this.nImmDaAnnotare = nImmDaAnnotare;
	}
	/**
	 * @return the nImmApprovate
	 */
	public int getnImmApprovate() {
		return nImmApprovate;
	}
	/**
	 * @param nImmApprovate the nImmApprovate to set
	 */
	public void setnImmApprovate(int nImmApprovate) {
		this.nImmApprovate = nImmApprovate;
	}
	/**
	 * @return the nImmRifiutate
	 */
	public int getnImmRifiutate() {
		return nImmRifiutate;
	}
	/**
	 * @param nImmRifiutate the nImmRifiutate to set
	 */
	public void setnImmRifiutate(int nImmRifiutate) {
		this.nImmRifiutate = nImmRifiutate;
	}
	/**
	 * @return the nSelezioniFatte
	 */
	public int getnSelezioniFatte() {
		return nSelezioniFatte;
	}
	/**
	 * @param nSelezioniFatte the nSelezioniFatte to set
	 */
	public void setnSelezioniFatte(int nSelezioniFatte) {
		this.nSelezioniFatte = nSelezioniFatte;
	}
	/**
	 * @return the nSelezioniDaFare
	 */
	public int getnSelezioniDaFare() {
		return nSelezioniDaFare;
	}
	/**
	 * @param nSelezioniDaFare the nSelezioniDaFare to set
	 */
	public void setnSelezioniDaFare(int nSelezioniDaFare) {
		this.nSelezioniDaFare = nSelezioniDaFare;
	}
	/**
	 * @return the totSelezioniFatte
	 */
	
	
	
}
