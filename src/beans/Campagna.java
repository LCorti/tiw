package beans;

import java.io.Serializable;

public class Campagna implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idCampagna;
	private int idGestore;
	private int parN;
	private int parM;
	private int parK;
	private boolean editabile;
	private boolean conclusa;
	private String titolo;
	
	public boolean isEditabile() {
		return editabile;
	}
	public void setEditabile(boolean editabile) {
		this.editabile = editabile;
	}
	public boolean isConclusa() {
		return conclusa;
	}
	public void setConclusa(boolean conclusa) {
		this.conclusa = conclusa;
	}
	private int dimLineaAnnotazione;
	/**
	 * @return the idCampagna
	 */
	public int getIdCampagna() {
		return idCampagna;
	}
	/**
	 * @param idCampagna the idCampagna to set
	 */
	public void setIdCampagna(int idCampagna) {
		this.idCampagna = idCampagna;
	}
	/**
	 * @return the idGestore
	 */
	public int getIdGestore() {
		return idGestore;
	}
	/**
	 * @param idGestore the idGestore to set
	 */
	public void setIdGestore(int idGestore) {
		this.idGestore = idGestore;
	}
	/**
	 * @return the parN
	 */
	public int getParN() {
		return parN;
	}
	/**
	 * @param parN the parN to set
	 */
	public void setParN(int parN) {
		this.parN = parN;
	}
	/**
	 * @return the parM
	 */
	public int getParM() {
		return parM;
	}
	/**
	 * @param parM the parM to set
	 */
	public void setParM(int parM) {
		this.parM = parM;
	}
	/**
	 * @return the parK
	 */
	public int getParK() {
		return parK;
	}
	/**
	 * @param parK the parK to set
	 */
	public void setParK(int parK) {
		this.parK = parK;
	}
	/**
	 * @return the titolo
	 */
	public String getTitolo() {
		return titolo;
	}
	/**
	 * @param titolo the titolo to set
	 */
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	/**
	 * @return the dimLineaAnnotazione
	 */
	public int getDimLineaAnnotazione() {
		return dimLineaAnnotazione;
	}
	/**
	 * @param dimLineaAnnotazione the dimLineaAnnotazione to set
	 */
	public void setDimLineaAnnotazione(int dimLineaAnnotazione) {
		this.dimLineaAnnotazione = dimLineaAnnotazione;
	}
}
