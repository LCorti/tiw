package beans;

import java.io.Serializable;
import java.sql.Date;

public class Annotazione implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idUtente;
	private int idImmagine;
	private String json;
	private Date timeStamp;
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return the idUtente
	 */
	public int getIdUtente() {
		return idUtente;
	}
	/**
	 * @param idUtente the idUtente to set
	 */
	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}
	/**
	 * @return the idImmagine
	 */
	public int getIdImmagine() {
		return idImmagine;
	}
	/**
	 * @param idImmagine the idImmagine to set
	 */
	public void setIdImmagine(int idImmagine) {
		this.idImmagine = idImmagine;
	}
	/**
	 * @return the json
	 */
	public String getJson() {
		return json;
	}
	/**
	 * @param json the json to set
	 */
	public void setJson(String json) {
		this.json = json;
	}
	
	
}
