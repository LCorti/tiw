package beans;

import java.io.Serializable;

public class Immagine implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idImmagine;
	private int idCampagna;
	private String path;
	private int nOk;
	private int nNo;
	private int nAnnotazioni;
	private boolean annotabile;
	private boolean selezioneFinita;
	private boolean annotazioneFinita;
	/**
	 * @return the idImmagine
	 */
	public int getIdImmagine() {
		return idImmagine;
	}
	/**
	 * @param idImmagine the idImmagine to set
	 */
	public void setIdImmagine(int idImmagine) {
		this.idImmagine = idImmagine;
	}
	/**
	 * @return the idCampagna
	 */
	public int getIdCampagna() {
		return idCampagna;
	}
	/**
	 * @param idCampagna the idCampagna to set
	 */
	public void setIdCampagna(int idCampagna) {
		this.idCampagna = idCampagna;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the nOk
	 */
	public int getnOk() {
		return nOk;
	}
	/**
	 * @param nOk the nOk to set
	 */
	public void setnOk(int nOk) {
		this.nOk = nOk;
	}
	/**
	 * @return the nNo
	 */
	public int getnNo() {
		return nNo;
	}
	/**
	 * @param nNo the nNo to set
	 */
	public void setnNo(int nNo) {
		this.nNo = nNo;
	}
	/**
	 * @return the nAnnotazioni
	 */
	public int getnAnnotazioni() {
		return nAnnotazioni;
	}
	/**
	 * @param nAnnotazioni the nAnnotazioni to set
	 */
	public void setnAnnotazioni(int nAnnotazioni) {
		this.nAnnotazioni = nAnnotazioni;
	}
	/**
	 * @return the annotabile
	 */
	public boolean isAnnotabile() {
		return annotabile;
	}
	/**
	 * @param annotabile the annotabile to set
	 */
	public void setAnnotabile(boolean annotabile) {
		this.annotabile = annotabile;
	}
	/**
	 * @return the selezioneFinita
	 */
	public boolean isSelezioneFinita() {
		return selezioneFinita;
	}
	/**
	 * @param selezioneFinita the selezioneFinita to set
	 */
	public void setSelezioneFinita(boolean selezioneFinita) {
		this.selezioneFinita = selezioneFinita;
	}
	/**
	 * @return the annotazioneFinita
	 */
	public boolean isAnnotazioneFinita() {
		return annotazioneFinita;
	}
	/**
	 * @param annotazioneFinita the annotazioneFinita to set
	 */
	public void setAnnotazioneFinita(boolean annotazioneFinita) {
		this.annotazioneFinita = annotazioneFinita;
	}
	
	
	
}
