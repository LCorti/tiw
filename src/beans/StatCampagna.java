package beans;

import java.io.Serializable;

public class StatCampagna implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int nImmaginiApprovate;
	private int nImmaginiRifiutate;
	private int nImmaginiAnnotate;
	private double mediaAnnotazioniPerImmagine;
	/**
	 * @return the nImmaginiApprovate
	 */
	public int getnImmaginiApprovate() {
		return nImmaginiApprovate;
	}
	/**
	 * @param nImmaginiApprovate the nImmaginiApprovate to set
	 */
	public void setnImmaginiApprovate(int nImmaginiApprovate) {
		this.nImmaginiApprovate = nImmaginiApprovate;
	}
	/**
	 * @return the nImmaginiRifiutate
	 */
	public int getnImmaginiRifiutate() {
		return nImmaginiRifiutate;
	}
	/**
	 * @param nImmaginiRifiutate the nImmaginiRifiutate to set
	 */
	public void setnImmaginiRifiutate(int nImmaginiRifiutate) {
		this.nImmaginiRifiutate = nImmaginiRifiutate;
	}
	/**
	 * @return the nImmaginiAnnotate
	 */
	public int getnImmaginiAnnotate() {
		return nImmaginiAnnotate;
	}
	/**
	 * @param nImmaginiAnnotate the nImmaginiAnnotate to set
	 */
	public void setnImmaginiAnnotate(int nImmaginiAnnotate) {
		this.nImmaginiAnnotate = nImmaginiAnnotate;
	}
	/**
	 * @return the mediaAnnotazioniPerImmagine
	 */
	public double getMediaAnnotazioniPerImmagine() {
		return mediaAnnotazioniPerImmagine;
	}
	/**
	 * @param d the mediaAnnotazioniPerImmagine to set
	 */
	public void setMediaAnnotazioniPerImmagine(double d) {
		this.mediaAnnotazioniPerImmagine = d;
	}
	
	
	
	
}
