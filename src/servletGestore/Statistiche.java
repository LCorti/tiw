package servletGestore;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Immagine;
import beans.StatCampagna;
import database.DBManager;

/**
 * Servlet implementation class Statistiche
 */
@WebServlet("/Statistiche")
public class Statistiche extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private DBManager db = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Statistiche() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(){
    	db = new DBManager();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		int idCampagna=Integer.parseInt(request.getParameter("idCampagna"));
		List<Immagine> immagini=new ArrayList<Immagine>();
		System.out.println("id passato" + idCampagna);
		
		StatCampagna stat=new StatCampagna();
		try {
			stat=db.getStatPerCampagna(idCampagna);
			immagini=db.getImmaginiCampagna(idCampagna);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("immagini", immagini);
		request.setAttribute("statistiche", stat);
		getServletContext().getRequestDispatcher("/StatPerImmaginiCampagna.jsp").forward(request, response);
		
		
		
		
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
