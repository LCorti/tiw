package servletGestore;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Annotazione;
import beans.Immagine;
import database.DBManager;

@WebServlet("/VisualizzaAnnotazioniServlet")
public class VisualizzaAnnotazioniServlet extends HttpServlet {
	private static final long serialVersionUID = 4470159285172578921L;

	private DBManager db = null;
	
	public VisualizzaAnnotazioniServlet() {
        super();
    }
    
    public void init(){
    	db = new DBManager();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		int campagna = Integer.parseInt(request.getParameter("idCampagna"));
		try {
			List<Immagine> immagini = db.getImmaginiAnnotate(campagna);
			List<Annotazione> annotazioni = db.getAnnotazioni(campagna);
			//System.out.println(immagini.size());
			//System.out.println(annotazioni.size());
			for(int i=0; i<annotazioni.size(); i++){
				System.out.println(annotazioni.get(i).getJson());
			}
			request.setAttribute("immaginiComplete", immagini);
			request.setAttribute("annotazioniComplete", annotazioni);
			getServletContext().getRequestDispatcher("/ImmaginiAnnotate.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
