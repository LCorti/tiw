package servletGestore;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import database.DBManager;

@WebServlet("/UploadServlet")
@MultipartConfig(maxFileSize=169999999)
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1391031395764124832L;
	
	private DBManager db = null;
	
	public UploadServlet() {
		super();
	}
	
	public void init(){
		db = new DBManager();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		
		List<Part> fileParts = request.getParts().stream().filter(part -> "file".equals(part.getName())).collect(Collectors.toList()); // Retrieves <input type="file" name="file" multiple="true">
		Image image;
		BufferedImage bImage;
		String path;
		String nomeFoto;
		File tmpFoto;
		Boolean complete = true;
		
		int idC = Integer.parseInt(request.getParameter("idCampagna"));
		InputStream inputstream = null;
		
		try{	
			for (Part filePart : fileParts){
				if(filePart!=null&&complete){
					inputstream = filePart.getInputStream();
					image = ImageIO.read(inputstream);
					bImage = this.createResizedCopy(image, true);
					path = request.getSession().getServletContext().getRealPath("");
					nomeFoto = "imagesTIW/photo"+(db.trovaUltimaFoto()+1)+".jpg";
					tmpFoto = new File(path+nomeFoto);
					ImageIO.write(bImage, "jpg", tmpFoto);
			
					complete = db.salvaImmagine(nomeFoto, idC);
				}
			}
			if(complete){
				response.sendRedirect("HomeGestore.jsp");
				return;
			} else {
				response.sendRedirect("DBErrorServlet");
				return;
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	BufferedImage createResizedCopy(Image originalImage, boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage ogBuffered = (BufferedImage) originalImage;
		int ogWidth = ogBuffered.getWidth();
		int ogHeight = ogBuffered.getHeight();
		int newWidth = ogWidth;
		int newHeight = ogHeight;
		if(ogWidth>=ogHeight){
			// orizziontale
			if(ogWidth>750){
				newWidth = 750;
				newHeight = (newWidth*ogHeight)/ogWidth;
			}
			if(newHeight>500){
				newHeight = 500;
				newWidth = (newHeight*ogWidth)/ogHeight;
			}
		} else {
			//verticale
			if(ogWidth>500){
				newWidth = 500;
				newHeight = (newWidth*ogHeight)/ogWidth;
			}
			if(newHeight>750){
				newHeight = 750;
				newWidth = (newHeight*ogWidth)/ogHeight;
			}
		}
		BufferedImage scaledBI = new BufferedImage(newWidth, newHeight, imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) {
			g.setComposite(AlphaComposite.Src);
		}
		g.drawImage(originalImage, 0, 0, newWidth, newHeight, null);
		g.dispose();
		return scaledBI;
	}
}