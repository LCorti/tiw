package servletGestore;

import java.io.IOException;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Campagna;
import database.DBManager;

/**
 * Servlet implementation class GeneraLavoratoriSelezioni
 */
@WebServlet("/GeneraLavoratoriSelezioni")
public class GeneraLavoratoriSelezioni extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private DBManager db = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GeneraLavoratoriSelezioni() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(){
    	db = new DBManager();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		Campagna c=null;
		//PrintWriter out=response.getWriter();
		
		try {
			c= db.getCampagnePerId(Integer.parseInt(request.getParameter("idCampagna")));
		} catch (NumberFormatException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int n=c.getParN();
		
		ArrayList<Integer> lavoratori=new ArrayList<Integer>();
		ArrayList<Integer> a1=new ArrayList<Integer>();
		
		try {
			a1=db.getlavoratoriOrderByImpegniAsc();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < a1.size() && n>0; i++,n--) {
			lavoratori.add(a1.get(i));
		}
		
		
		
		if(n>0){			
			response.sendRedirect("NonAbbiamoAbbastanzaLavoratori.html");
			System.out.println("non ci sono abbastanza lavoratori");
			return;
		}
		

		for(int i = 0; i < lavoratori.size(); i++) {   
		    System.out.print(lavoratori.get(i));
		}  	 	
		
		
		Integer[] totLavoratori = new Integer[lavoratori.size()];
		for(int i = 0; i < lavoratori.size(); i++) {   
			totLavoratori[i]=lavoratori.get(i);
		}
		
		db.insertLavoratori(Integer.parseInt(request.getParameter("idCampagna")), totLavoratori, "GruppoSel");
		
		System.out.println("Lavoratori assegnati!");
		response.sendRedirect("HomeGestore.jsp");
		
		
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
