package servletGestore;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import checks.StatusCampagna;
import database.DBManager;

/**
 * Servlet implementation class InfoCampagna
 */
@WebServlet("/InfoCampagna")
public class InfoCampagna extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private DBManager db = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfoCampagna() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init(){
    	db = new DBManager();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		PrintWriter out=response.getWriter();
		boolean canBeActivated=true;
		
		int idCampagna=Integer.parseInt(request.getParameter("idCampagna"));
		System.out.println("id passato" + idCampagna);
		
		
		StatusCampagna sc=db.checkStatusCampagna(idCampagna);
		
		

		//controllo presenza immagini relative alla campagna selezionata
		
		if(!sc.isPresenzaImmagini()){
			//da modificare la action e passare l'id della campagna
			String s="<form action='UploadImmagini.jsp'><input type='hidden' value='"+idCampagna+"' name='idCampagna' />azione consigliata: <input type='submit' value='Carica Immagini' /></form>";
			out.print(s);
			canBeActivated=false;
		}
		
		//controllo presenza lavoratori selezione relative alla campagna selezionata
		
		if(!sc.isPresenzaLavSel()){
			String s;
			if (sc.isAbbastanzaLavSel()) {
				//da modificare la action e passare l'id della campagna
				s = "<form action='AggiungiLavoratoreSelezioni.jsp'><input type='hidden' value='"
						+ idCampagna
						+ "' name='idCampagna' />azione consigliata: <input type='submit' value='Scegli lavoratori per il task di selezione' /></form>";
				
						
				if (canBeActivated) {
					s += "<br><br><br>" + "<form action='UploadImmagini.jsp'>" + "<input type='hidden' value='"
							+ idCampagna + "' name='idCampagna' />"
							+ "azione opzionale: <input type='submit' value='Carica Immagini' />" + "</form>";
				}
			}
			else{
				s = "<br> siamo spiacenti ma sulla nostra piattaforma non ci sono ancora abbastanza lavoratori per il task di Selezione<br>";
				}
			out.print(s);
			canBeActivated = false;
		}
		
		
		//controllo presenza lavoratori Annotazione alla campagna selezionata
	
		if(!sc.isPresenzaLavAnn()){
			String s;
			if (sc.isAbbastanzaLavAnn()) {
				s = "<form action='AggiungiLavoratoreAnnotazioni.jsp'><input type='hidden' value='"
						+ idCampagna
						+ "' name='idCampagna' />azione consigliata: <input type='submit' value='Scegli lavoratori per il task di Annotazione' />"
						+ "</form> ";
				if (canBeActivated) {
					s += "<br><br><br>" + "<form action='UploadImmagini.jsp'>" + "<input type='hidden' value='"
							+ idCampagna + "' name='idCampagna' />"
							+ "azione opzionale: <input type='submit' value='Carica Immagini' />" + "</form>";
				}
			}
			else{
				s = "<br>siamo spiacenti ma sulla nostra piattaforma non ci sono ancora abbastanza lavoratori per il task di Annotazione<br>";
			}
			out.print(s);
			canBeActivated=false;
		}
		
		
		
		//controllo che non sia ancora editabile e che sia attivabile
		
		if(sc.isEditabile()&&canBeActivated){
			//da modificare la action e passare l'id della campagna
			String s="<form action='AvviaCampagna'><input type='hidden' value='"+idCampagna+"' name='idCampagna' />azione consigliata: <input type='submit' value='Avvia Campagna' /><br>Attenzione! una volta che attivi la campagna non potrai piu' modificarla!</form>";
			out.print(s);
			s="<br><br><br><form action='UploadImmagini.jsp'><input type='hidden' value='"+idCampagna+"' name='idCampagna' />azione opzionale: <input type='submit' value='Carica Immagini' /></form>";
			out.print(s);
			canBeActivated=false;
		}
		
		
		//stampo statistiche
	
		if(!sc.isEditabile()){
			String s = "<form action='Statistiche' method='get'>"
					+ "<input type='hidden' value='"
					+ idCampagna
					+ "' name='idCampagna' />azione consigliata: <input type='submit' value='Visualizza Statistiche Accurate' />"
					+ "</form><br>";
			if (sc.isEsistonoImmaginiAnnotate()) {
				s += "<form action='VisualizzaAnnotazioniServlet' method='get'>" + "<input type='hidden' value='"
						+ idCampagna
						+ "' name='idCampagna' />azione consigliata: <input type='submit' value='Visualizza annotazioni' />"
						+ "</form><br> ";
			}
			out.print(s);
		}	
		
		
		String elimina="<form action='EliminaCampagna' method='get'>"
				+ "<br><br><input type='hidden' value='"
				+ idCampagna
				+ "' name='idCampagna' /><input type='submit' value='Elimina Campagna' />"
				+ "</form> ";
		
		out.print(elimina);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
