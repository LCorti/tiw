package servletGestore;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBManager;

/**
 * Servlet implementation class AvviaCampagna
 */
@WebServlet("/AvviaCampagna")
public class AvviaCampagna extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private DBManager db = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AvviaCampagna() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init(){
    	db = new DBManager();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		
		PrintWriter out=response.getWriter();
		int idCampagna=Integer.parseInt(request.getParameter("idCampagna"));
		System.out.println("id passato" + idCampagna);
		
		try {
			db.avviaCampagna(idCampagna);
			out.print("<head><meta http-equiv='refresh' content='5;url=HomeGestore.jsp' /></head><body>	Campagna avviata con successo! ora puoi controllare lo stato di avanzamento tramite le statistiche fornite nella tua pagina <a href='HomeGestore.jsp'>Home!</a><br><br>verrai reindirizzato in automatico tra 8 secondi... </body></html>");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
