package servletGestore;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBManager;

/**
 * Servlet implementation class RegistraLavoratori
 */
@WebServlet("/RegistraLavoratori")
public class RegistraLavoratori extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private DBManager db = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistraLavoratori() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void init(){
    	db = new DBManager();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		String[] l= request.getParameterValues("lavoratori");
		String tipo=request.getParameter("tipo");
		int idCampagna= Integer.parseInt(request.getParameter("idCampagna"));
		//System.out.println(Arrays.toString(l));
		
		//converto arrau da stringa a integer
		Integer[] lavoratori = new Integer[l.length];
		for(int i = 0; i < l.length; i++) {   
			lavoratori[i]=Integer.parseInt(l[i]);
		}
		
		
		switch (tipo) {
		case "sel":
			db.insertLavoratori(idCampagna, lavoratori,"GruppoSel");
			break;
		case "ann":
			db.insertLavoratori(idCampagna, lavoratori,"GruppoAnn");
			break;
			
		default:
			break;
		}
		
		response.sendRedirect("HomeGestore.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
