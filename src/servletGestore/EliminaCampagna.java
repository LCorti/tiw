package servletGestore;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBManager;

/**
 * Servlet implementation class EliminaCampagna
 */
@WebServlet("/EliminaCampagna")
public class EliminaCampagna extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	private DBManager db = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EliminaCampagna() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init(){
    	db = new DBManager();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		
		int idCampagna=Integer.parseInt(request.getParameter("idCampagna"));
		System.out.println("id passato" + idCampagna);
		try {
			db.eliminaCampagna(idCampagna);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		response.sendRedirect("HomeGestore.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
