package servletGestore;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.Campagna;
import beans.Utente;
import database.DBManager;

@WebServlet("/RegistrazioneCampagna")
public class RegistrazioneCampagna extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private DBManager db = null;
	
    public RegistrazioneCampagna() {
        super();
    }

    public void init(){
    	db = new DBManager();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		HttpSession s=request.getSession();
		Utente u= (Utente) s.getAttribute("user");
		
		
		
		Campagna c= new Campagna();
		c.setTitolo(request.getParameter("titolo"));
		c.setIdGestore(u.getIdUtente());
		c.setParK(Integer.parseInt(request.getParameter("parK")) );
		c.setParN(Integer.parseInt(request.getParameter("parN")) );
		c.setParM(Integer.parseInt(request.getParameter("parM")) );
		c.setEditabile(true);
		c.setConclusa(false);
		c.setDimLineaAnnotazione(Integer.parseInt(request.getParameter("dimLineaAnnotazione")));
		
		try {
			db.insertCampagna(c);
			response.sendRedirect("HomeGestore.jsp");
		} catch (SQLException e) {
			//pagina del bordello
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
