package servletAuth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Campagna;
import beans.Utente;
import database.DBManager;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    
	private static final long serialVersionUID = -1078398052783548639L;
	private DBManager db = null;
	
    public LoginServlet() {
        super();
    }

    public void init() throws ServletException{
    	//db = DBManager.getInstance();
    	db = new DBManager();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		try {
			String user = request.getParameter("usr");
			String password = request.getParameter("pwd");
			Utente utente = db.checkUser(user, password);
			HttpSession session = request.getSession();
			session.setAttribute("user", utente);
			
			List<Campagna> camp = new ArrayList<Campagna>();
			
			/*if("gestore".equals(utente.getRuolo())){
				camp = db.getCampagnePerGestore(utente);
				session.setAttribute("campagne", camp);
				response.sendRedirect("HomeGestore.jsp");
			} else*/ if("lavoratore".equals(utente.getRuolo())){
				camp = db.recuperaCampLav(utente.getIdUtente());
				session.setAttribute("campagne", camp);
				response.sendRedirect("HomeLavoratore.jsp");
			} else {
				response.sendRedirect("login.jsp?err=auth");
			}
		} catch (Exception e) {
			response.sendRedirect("login.jsp?err=exc");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
}
