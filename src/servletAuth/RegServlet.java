package servletAuth;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Utente;
import database.DBManager;

@WebServlet("/RegServlet")
public class RegServlet extends HttpServlet {
    
	private static final long serialVersionUID = 7173963257178025541L;
	private DBManager db = null;
	
    public RegServlet() {
        super();
    }
    
    public void init() throws ServletException{
    	//db = DBManager.getInstance();
    	db = new DBManager();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		try {
			Utente user = new Utente();
			user.setUsername(request.getParameter("username"));
			user.setPassword(request.getParameter("password"));
			user.setNome(request.getParameter("nome"));
			user.setCognome(request.getParameter("cognome"));
			user.setRuolo(request.getParameter("ruolo"));
			if(db.registraUser(user)){
				response.sendRedirect("login.jsp");
			}else{
				response.sendRedirect("login.jsp?err=reg");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("login.jsp?err=exc");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
