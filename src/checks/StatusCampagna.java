package checks;

public class StatusCampagna {
	private boolean editabile;
	private boolean presenzaImmagini;
	private boolean presenzaLavSel;
	private boolean presenzaLavAnn;
	private boolean abbastanzaLavAnn;
	private boolean abbastanzaLavSel;
	private boolean esistonoImmaginiAnnotate;
	
	/**
	 * @return the editabile
	 */
	public boolean isEditabile() {
		return editabile;
	}
	/**
	 * @param editabile the editabile to set
	 */
	public void setEditabile(boolean editabile) {
		this.editabile = editabile;
	}
	/**
	 * @return the presenzaImmagini
	 */
	public boolean isPresenzaImmagini() {
		return presenzaImmagini;
	}
	/**
	 * @param presenzaImmagini the presenzaImmagini to set
	 */
	public void setPresenzaImmagini(boolean presenzaImmagini) {
		this.presenzaImmagini = presenzaImmagini;
	}
	/**
	 * @return the presenzaLavSel
	 */
	public boolean isPresenzaLavSel() {
		return presenzaLavSel;
	}
	/**
	 * @param presenzaLavSel the presenzaLavSel to set
	 */
	public void setPresenzaLavSel(boolean presenzaLavSel) {
		this.presenzaLavSel = presenzaLavSel;
	}
	/**
	 * @return the presenzaLavAnn
	 */
	public boolean isPresenzaLavAnn() {
		return presenzaLavAnn;
	}
	/**
	 * @param presenzaLavAnn the presenzaLavAnn to set
	 */
	public void setPresenzaLavAnn(boolean presenzaLavAnn) {
		this.presenzaLavAnn = presenzaLavAnn;
	}
	/**
	 * @return the abbastanzaLavAnn
	 */
	public boolean isAbbastanzaLavAnn() {
		return abbastanzaLavAnn;
	}
	/**
	 * @param abbastanzaLavAnn the abbastanzaLavAnn to set
	 */
	public void setAbbastanzaLavAnn(boolean abbastanzaLavAnn) {
		this.abbastanzaLavAnn = abbastanzaLavAnn;
	}
	/**
	 * @return the abbastanzaLavSel
	 */
	public boolean isAbbastanzaLavSel() {
		return abbastanzaLavSel;
	}
	/**
	 * @param abbastanzaLavSel the abbastanzaLavSel to set
	 */
	public void setAbbastanzaLavSel(boolean abbastanzaLavSel) {
		this.abbastanzaLavSel = abbastanzaLavSel;
	}
	/**
	 * @return the esistonoImmaginiAnnotate
	 */
	public boolean isEsistonoImmaginiAnnotate() {
		return esistonoImmaginiAnnotate;
	}
	/**
	 * @param esistonoImmaginiAnnotate the esistonoImmaginiAnnotate to set
	 */
	public void setEsistonoImmaginiAnnotate(boolean esistonoImmaginiAnnotate) {
		this.esistonoImmaginiAnnotate = esistonoImmaginiAnnotate;
	}
	
}
