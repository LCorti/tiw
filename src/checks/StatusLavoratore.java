package checks;

public class StatusLavoratore {

	private boolean sel;
	private boolean ann;
	
	public StatusLavoratore() {
	}
	
	public boolean getSel() {
		return sel;
	}

	public void setSel(boolean sel) {
		this.sel = sel;
	}

	public boolean getAnn() {
		return ann;
	}

	public void setAnn(boolean ann) {
		this.ann = ann;
	}

}
