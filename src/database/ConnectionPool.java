package database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;

public class ConnectionPool {

	private static ConnectionPool pool = null;
	private static DataSource dataSource = null;
	
	static final String loginUser = "tiwUser";
	static final String loginPasswd = "tiwuser2017";
	static final String driver = "com.mysql.jdbc.Driver";
	static final String url = "jdbc:mysql://localhost:3306/mydb?useSSL=false";
	
	private ConnectionPool() {
		try{
			dataSource = setupDataSource(driver, url, loginUser, loginPasswd);
		} catch(Exception e){
			
		}
	}

	public static ConnectionPool getInstance(){
		if(pool==null){
			pool = new ConnectionPool();
		}
		return pool;
	}
	
	public static DataSource setupDataSource(String driver, String connectURI, String username, String password) {
    	BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(driver);
        basicDataSource.setUsername(username);
        basicDataSource.setPassword(password);
        basicDataSource.setUrl(connectURI);       
        return basicDataSource;
    }
	
	public Connection getConnection(){
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			//sfbdfhb
			e.printStackTrace();
			return null;
		}
	}
	
	public void freeConnection(Connection c){
		try {
			c.close();
		} catch (SQLException e) {
			// dghdfg
			e.printStackTrace();
		}
	}
	
	public static void printDataSourceStats(DataSource dataSource) throws SQLException {
        BasicDataSource basicDataSource = (BasicDataSource) dataSource;
        System.out.println("NumActive: " + basicDataSource.getNumActive());
        System.out.println("NumIdle: " + basicDataSource.getNumIdle());
    }
}
