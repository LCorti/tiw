package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import beans.Annotazione;
import beans.Campagna;
import beans.Immagine;
import beans.Utente;
import checks.StatusCampagna;
import checks.StatusLavoratore;
import beans.StatCampagna;
import beans.StatLavoratore;

public class DBManager {

	//private static DBManager instance = null;

	public DBManager() {
	}

	/*public static DBManager getInstance(){
		if(instance==null){
			instance = new DBManager();
		}
		return instance;
	}*/

	public Utente checkUser(String username, String password) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Utente user = new Utente();
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "SELECT * FROM Utente WHERE username = ? AND password = ?";
			ps = conn.prepareStatement(query);
			ps.setString(1, username);
			ps.setString(2, password);
			rs = ps.executeQuery();
			if(rs.next()){
				user.setIdUtente(rs.getInt("idUtente"));
				user.setUsername(rs.getString("username"));
				user.setNome(rs.getString("nome"));
				user.setCognome(rs.getString("cognome"));
				user.setRuolo(rs.getString("ruolo"));
			}
			return user;
		} catch (Exception e) {
			return null;
		} finally{
			rs.close();
			ps.close();
			cp.freeConnection(conn);
		}
	}
	
	public boolean registraUser(Utente user) throws SQLException {
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement psEsiste = null;
		ResultSet rsEsiste = null;
		PreparedStatement psIns = null;
		try {
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String esiste = "SELECT * FROM Utente WHERE username=?";
			psEsiste = conn.prepareStatement(esiste);
			psEsiste.setString(1, user.getUsername());
			rsEsiste = psEsiste.executeQuery();
			if(!rsEsiste.next()){
				String insertNew = "INSERT INTO Utente(username,password,nome,cognome,ruolo)"
						+ "VALUES (?,?,?,?,?)";
				psIns = conn.prepareStatement(insertNew);
				psIns.setString(1, user.getUsername());
				psIns.setString(2, user.getPassword());
				psIns.setString(3, user.getNome());
				psIns.setString(4, user.getCognome());
				psIns.setString(5, user.getRuolo());
				psIns.executeUpdate();
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			rsEsiste.close();
			psEsiste.close();
			psIns.close();
			cp.freeConnection(conn);
		}
	}
	
	public List<Campagna> recuperaCampLav(int idUsr) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement psCamp = null;
		ResultSet rsCamp = null;
		try {
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			List<Campagna> c = new ArrayList<Campagna>();
			String queryCamp = "SELECT * FROM Campagna WHERE idCampagna IN (SELECT idCampagna FROM GruppoSel WHERE idUtente = ?)"
					+ "OR idCampagna IN (SELECT idCampagna FROM GruppoAnn WHERE idUtente = ?) AND editabile = 0 AND conclusa = 0";
			psCamp = conn.prepareStatement(queryCamp);
			psCamp.setInt(1, idUsr);
			psCamp.setInt(2, idUsr);
			rsCamp = psCamp.executeQuery();
			while(rsCamp.next()){
				Campagna cTemp = new Campagna();
				cTemp.setIdCampagna(rsCamp.getInt("idCampagna"));
				cTemp.setTitolo(rsCamp.getString("titolo"));
				cTemp.setIdGestore(idUsr);
				cTemp.setParN(rsCamp.getInt("parN"));
				cTemp.setParK(rsCamp.getInt("parK"));
				cTemp.setParM(rsCamp.getInt("parM"));
				cTemp.setEditabile(rsCamp.getBoolean("editabile"));
				cTemp.setConclusa(rsCamp.getBoolean("conclusa"));
				cTemp.setDimLineaAnnotazione(rsCamp.getInt("dimLineaAnnotazione"));
				c.add(cTemp);
			}
			return c;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			psCamp.close();
			rsCamp.close();
			cp.freeConnection(conn);
		}
	}
	
	public ArrayList<Campagna> getCampagnePerGestore(Utente u) throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		ArrayList<Campagna> c = new ArrayList<Campagna>();;
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			
			String query = "SELECT * FROM CAMPAGNA WHERE IDGESTORE= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, u.getIdUtente());
			res= stmt.executeQuery();
			while(res.next()){
				Campagna campagna=new Campagna();	
				campagna.setIdCampagna(res.getInt("idCampagna"));
				campagna.setIdGestore(res.getInt("idGestore"));
				campagna.setParK(res.getInt("parK"));
				campagna.setParN(res.getInt("parN"));
				campagna.setParM(res.getInt("parM"));
				campagna.setTitolo(res.getString("titolo"));
				campagna.setEditabile(res.getBoolean("editabile"));
				campagna.setConclusa(res.getBoolean("conclusa"));
				campagna.setDimLineaAnnotazione(res.getInt("dimLineaAnnotazione"));
				c.add(campagna);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
		return c;
	}
	
	public Boolean insertCampagna(Campagna c) throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement stmt = null;
		try {
			String query = "INSERT INTO Campagna (titolo,idGestore,parN,parK,parM,editabile,conclusa,dimLineaAnnotazione) VALUES (?,?,?,?,?,?,?,?)";
			stmt= connection.prepareStatement(query);
			stmt.setString(1,c.getTitolo());
			stmt.setInt(2, c.getIdGestore());
			stmt.setInt(3, c.getParN());
			stmt.setInt(4, c.getParK());
			stmt.setInt(5, c.getParM());
			stmt.setBoolean(6, c.isEditabile());
			stmt.setBoolean(7, c.isConclusa());
			stmt.setInt(8, c.getDimLineaAnnotazione());
			stmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			stmt.close();
			pool.freeConnection(connection);
		}		
	}
	
	public StatCampagna getStatPerCampagna(int idCampagna) throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		StatCampagna stat=new StatCampagna();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query=null;
		try {
			
			query = "SELECT numimmannotate FROM statnimmannotatepercampagna WHERE IDCampagna= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idCampagna);
			rs= stmt.executeQuery();
			if(rs.next()){
				stat.setnImmaginiAnnotate(rs.getInt(1));
			}
			else{
				stat.setnImmaginiAnnotate(0);
			}
			
			
			query = "SELECT nimmok FROM statimmokpercampagna WHERE IDCampagna= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idCampagna);
			rs= stmt.executeQuery();
			if(rs.next()){
				stat.setnImmaginiApprovate(rs.getInt(1));
			}
			else{
				stat.setnImmaginiApprovate(0);
			}
			
			
			
			query = "SELECT nimmrifiutate FROM statnimmrifiutatepercampagna WHERE IDCampagna= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idCampagna);
			rs= stmt.executeQuery();
			if(rs.next()){
				stat.setnImmaginiRifiutate(rs.getInt(1));
			}
			else{
				stat.setnImmaginiRifiutate(0);
			}
			
			
			
			query = "SELECT mediaAnn FROM statavgannotazionipercampagna WHERE IDCampagna= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idCampagna);
			rs= stmt.executeQuery();
			if(rs.next()){
				stat.setMediaAnnotazioniPerImmagine(rs.getDouble(1));
			}
			else{
				stat.setMediaAnnotazioniPerImmagine(0);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			rs.close();
			pool.freeConnection(connection);
		}
		
		return stat;
	}
	
	public int trovaUltimaFoto() throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		int id = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "SELECT max(idImmagine) AS UltimaFoto FROM Immagine";
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			if(rs.next()){
				id = rs.getInt("UltimaFoto");
			}
		} catch (Exception e){
			e.printStackTrace();
		} finally{
			rs.close();
			ps.close();
			cp.freeConnection(conn);
		}
		return id;
	}
	
	public boolean salvaImmagine(String nome, int campagna) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement psInsert = null;
		try{
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String newImageIns = "INSERT INTO Immagine (idCampagna, path) VALUES (?,?)";
			psInsert = conn.prepareStatement(newImageIns);
			psInsert.setInt(1, campagna);
			psInsert.setString(2, nome);
			psInsert.executeUpdate();
			return true;
		} catch(Exception e){
			e.printStackTrace();
			return false;
		} finally {
			psInsert.close();
			cp.freeConnection(conn);
		}
		
	}

	public StatusCampagna checkStatusCampagna(int idCampagna){
		StatusCampagna sc=new StatusCampagna();
		ConnectionPool cp = null;
		Connection conn = null;
		try {
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			sc.setEditabile(checkEditabile(idCampagna, conn));
			sc.setPresenzaImmagini(checkPresenzaImmagini(idCampagna, conn));
			sc.setPresenzaLavAnn(checkPresenzaLavoratoriAannotazione(idCampagna, conn));
			sc.setPresenzaLavSel(checkPresenzaLavoratoriSelezione(idCampagna, conn));
			sc.setAbbastanzaLavSel(checkSeAbbastanzaLavoratoriSelezione(idCampagna, conn));
			sc.setAbbastanzaLavAnn(checkSeAbbastanzaLavoratoriAnnotazione(idCampagna, conn));
			sc.setEsistonoImmaginiAnnotate(checkExistImmaginiAnnotate(idCampagna, conn));
			return sc;
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			cp.freeConnection(conn);
		}
		
		return sc;
	}

	public boolean checkPresenzaImmagini(int idCampagna, Connection conn) throws SQLException{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM IMMAGINE WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			rs.close();
			
		}
		
		return false;
	}
	
	private boolean checkPresenzaLavoratoriSelezione(int idCampagna,Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM GRUPPOSEL WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			rs.close();
			
		}
		
		return false;
	}
	
	private boolean checkPresenzaLavoratoriAannotazione(int idCampagna, Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM GRUPPOANN WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			rs.close();
		}
		
		return false;
	}
	
	private boolean checkEditabile(int idCampagna, Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT EDITABILE FROM CAMPAGNA WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return rs.getBoolean("editabile");
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			rs.close();
		}
		
		return false;
	}
	
	private boolean checkSeAbbastanzaLavoratoriSelezione(int idCampagna,Connection conn) throws SQLException{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			String query = "SELECT * FROM abbastanzalavoratoridisponibiliselezione WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			rs.close();
			
		}
		
		return false;
	}
	
	private boolean checkSeAbbastanzaLavoratoriAnnotazione(int idCampagna,Connection conn) throws SQLException{
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
		
			String query = "SELECT * FROM abbastanzalavoratoridisponibiliannotazione WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			rs.close();
			
		}
		
		return false;
	}
	
	private boolean checkExistImmaginiAnnotate(int idCampagna,Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM Immagine WHERE IDCAMPAGNA=? and nAnnotazioni>0";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			
			rs.close();
			ps.close();
		}
		
		return false;
	}

	public Campagna getCampagnePerId(int idCampagna) throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		Campagna campagna=new Campagna();
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			
			String query = "SELECT * FROM CAMPAGNA WHERE IDCAMPAGNA= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idCampagna);
			res= stmt.executeQuery();
			while(res.next()){
					
				campagna.setIdCampagna(res.getInt("idCampagna"));
				campagna.setIdGestore(res.getInt("idGestore"));
				campagna.setParK(res.getInt("parK"));
				campagna.setParN(res.getInt("parN"));
				campagna.setParM(res.getInt("parM"));
				campagna.setTitolo(res.getString("titolo"));
				campagna.setEditabile(res.getBoolean("editabile"));
				campagna.setConclusa(res.getBoolean("conclusa"));
				campagna.setDimLineaAnnotazione(res.getInt("dimLineaAnnotazione"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
		return campagna;
	}
	
	public List<Utente> getLavoratori() throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		List<Utente> lu = null;
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			lu = new ArrayList<Utente>();
			String query = "SELECT * FROM UTENTE WHERE ruolo='lavoratore'";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			res=stmt.executeQuery();
			while(res.next()){
				Utente u=new Utente();
				
				u.setIdUtente(res.getInt("idUtente"));
				u.setCognome(res.getString("cognome"));
				u.setNome(res.getString("nome"));
				u.setUsername(res.getString("username"));
				u.setRuolo(res.getString("ruolo"));
				u.setPassword(res.getString("password"));
				
				lu.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
		
		
		return lu;
		
	}

	public int getParNCampagna(int idCampagna) throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		int i = 0;
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			String query = "SELECT parN FROM CAMPAGNA WHERE idCampagna=?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idCampagna);
			res=stmt.executeQuery();
			while(res.next()){
				i=res.getInt("parN");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
		return i;
	}
	
	public int getParMCampagna(int idCampagna) throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		int i = 0;
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			String query = "SELECT parM FROM CAMPAGNA WHERE idCampagna=?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idCampagna);
			res=stmt.executeQuery();
			while(res.next()){
				i=res.getInt("parM");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
		return i;
	}
	
	public boolean insertLavoratori(int idCampagna, Integer[] lavoratori, String tabella){
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement stmt = null;
		try {
			
			for(int i=0;i<lavoratori.length;i++){
				String query = "INSERT INTO "+ tabella +" (idCampagna,idUtente) VALUES (?,?)";
				stmt= connection.prepareStatement(query);
				stmt.setInt(1, idCampagna);
				stmt.setInt(2, lavoratori[i]);
				stmt.executeUpdate();
				stmt.close();
			}
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			pool.freeConnection(connection);
		}		
		
		return false;
	}
	
	public ArrayList<Integer> getLavoratoriMaiAssegnatiSelezione() throws SQLException{
		ArrayList<Integer> lav=new ArrayList<Integer>();
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			String query = "SELECT * FROM LAVORATORIMAIASSEGNATISELEZIONE";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			res=stmt.executeQuery();
			while(res.next()){
				lav.add(res.getInt("idUtente"));
			
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
				
		return lav;
	}
	
	public ArrayList<Integer> getLavoratoriMaiAssegnatiAnnotazione() throws SQLException{
		ArrayList<Integer> lav=new ArrayList<Integer>();
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			String query = "SELECT * FROM LAVORATORIMAIASSEGNATIANNOTAZIONE";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			res=stmt.executeQuery();
			while(res.next()){
				lav.add(res.getInt("idUtente"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
				
		return lav;
	}
	
	public ArrayList<Integer> getlavoratoriOrderByImpegniAsc() throws SQLException{
		ArrayList<Integer> lav=new ArrayList<Integer>();
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		
		PreparedStatement stmt = null;
		ResultSet res = null;
		try {
			String query = "SELECT idUtente FROM totcompitiperlavoratore";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			res=stmt.executeQuery();
			while(res.next()){
				lav.add(res.getInt("idUtente"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			res.close();
			pool.freeConnection(connection);
		}
				
		return lav;
	}
	
	private Immagine getImmagine(int idCampagna, int idUtente, Connection conn, String query)throws SQLException{
		Immagine image = new Immagine();
		PreparedStatement psImg = null;
		ResultSet rsImg = null;
		psImg = conn.prepareStatement(query);
		psImg.setInt(1, idCampagna);
		psImg.setInt(2, idUtente);
		rsImg = psImg.executeQuery();
		if(rsImg.next()){
			image.setIdImmagine(rsImg.getInt("idImmagine"));
			image.setIdCampagna(idCampagna);
			image.setPath(rsImg.getString("path"));
		} else {
			image.setPath("");
		}
		return image;
	}
	
	public Immagine getImmagineSel(int idCampagna, int idUtente, Connection conn) throws SQLException{
		Immagine image = new Immagine();
		ConnectionPool cp = null;
		//Connection conn = null;
		//PreparedStatement psImg = null;
		//ResultSet rsImg = null;
		try{
			/*cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String queryImgSel = "SELECT idImmagine, path, (nOk+nNo) AS VotiFoto "
					+ "FROM Immagine WHERE idCampagna = ? AND idImmagine NOT IN "
					+ "(SELECT idImmagine FROM Selezione WHERE idUtente = ?) "
					+ "ORDER BY VotiFoto";
			psImg = conn.prepareStatement(queryImgSel);
			psImg.setInt(1, idCampagna);
			psImg.setInt(2, idUtente);
			rsImg = psImg.executeQuery();
			//prelevo solo la prima
			if(rsImg.next()){
				image.setIdImmagine(rsImg.getInt("idImmagine"));
				image.setIdCampagna(idCampagna);
				image.setPath(rsImg.getString("path"));
				//image = rsImg.getString("path");
			} else {
				image.setPath("");
			}*/
			
			if(conn==null){
				cp = ConnectionPool.getInstance();
				conn = cp.getConnection();
			}
			String queryImgSel = "SELECT idImmagine, path, (nOk+nNo) AS VotiFoto "
					+ "FROM Immagine WHERE idCampagna = ? AND idImmagine NOT IN "
					+ "(SELECT idImmagine FROM Selezione WHERE idUtente = ?) "
					+ "ORDER BY VotiFoto";
			
			image = getImmagine(idCampagna, idUtente, conn, queryImgSel);
			
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			//rsImg.close();
			//psImg.close();
			if(cp!=null){
				cp.freeConnection(conn);
			}
		}
		return image;
	}
	
	public Immagine getImmagineAnn(int idCampagna, int idUtente, Connection conn) throws SQLException{
		Immagine image = new Immagine();
		ConnectionPool cp = null;
		//Connection conn = null;
		//PreparedStatement psImg = null;
		//ResultSet rsImg = null;
		try{
			/*cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String queryImgAnn = "SELECT idImmagine, path FROM Immagine WHERE idCampagna = ? AND annotabile=1 AND idImmagine "
					+ "NOT IN (SELECT idImmagine FROM Annotazione WHERE idUtente = ?) "
					+ "ORDER BY nAnnotazioni";
			psImg = conn.prepareStatement(queryImgAnn);
			psImg.setInt(1, idCampagna);
			psImg.setInt(2, idUtente);
			rsImg = psImg.executeQuery();
			if(rsImg.next()){
				image.setIdImmagine(rsImg.getInt("idImmagine"));
				image.setIdCampagna(idCampagna);
				image.setPath(rsImg.getString("path"));
			} else {
				image.setPath("");
			}*/
			
			if(conn==null){
				cp = ConnectionPool.getInstance();
				conn = cp.getConnection();
			}
			String queryImgAnn = "SELECT idImmagine, path FROM Immagine WHERE idCampagna = ? AND annotabile=1 AND idImmagine "
					+ "NOT IN (SELECT idImmagine FROM Annotazione WHERE idUtente = ?) "
					+ "ORDER BY nAnnotazioni";
			image = getImmagine(idCampagna, idUtente, conn, queryImgAnn);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			//rsImg.close();
			//psImg.close();
			if(cp!=null){
				cp.freeConnection(conn);
			}
		}
		return image;
	}
	
	public void salvaEsito(int immagine, int user, int esito) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement newVoto = null;
		try{
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String insertVoto = "INSERT INTO Selezione (idUtente, idImmagine, esito) VALUES (?,?,?)";
			newVoto = conn.prepareStatement(insertVoto);
			newVoto.setInt(1, user);
			newVoto.setInt(2, immagine);
			newVoto.setInt(3, esito);
			newVoto.executeUpdate();
		}catch(Exception e){
			
		}finally{
			newVoto.close();
			cp.freeConnection(conn);
		}
	}
	
	public boolean avviaCampagna(int idCampagna) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "UPDATE Campagna SET editabile=0 WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			ps.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			cp.freeConnection(conn);
		}
		
		return false;		
	}
	
	public ArrayList<Immagine> getImmaginiCampagna(int idCampagna) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Immagine> immagini=new ArrayList<Immagine>();
		try {
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "Select * from immagine WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			while(rs.next()){
				Immagine imm=new Immagine();
				imm.setAnnotabile(rs.getBoolean("annotabile"));
				imm.setIdCampagna(idCampagna);
				imm.setIdImmagine(rs.getInt("idImmagine"));
				imm.setnAnnotazioni(rs.getInt("nAnnotazioni"));
				imm.setnOk(rs.getInt("nOk"));
				imm.setnNo(rs.getInt("nNo"));
				imm.setAnnotazioneFinita(rs.getBoolean("annotazioneFinita"));
				imm.setSelezioneFinita(rs.getBoolean("selezioneFinita"));
				imm.setPath(rs.getString("path"));
				
				immagini.add(imm);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			cp.freeConnection(conn);
		}
		
		return immagini;
		
	}
		
	public void salvaAnnotazione(int user, int imm, String annotazione) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement psAnn = null;
		Date data = new Date();
		Timestamp timestamp = new Timestamp(data.getTime());
		try{
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String newAnn = "INSERT INTO Annotazione (idUtente,idImmagine,annData,timeStamp) VALUES (?,?,?,?)";
			psAnn = conn.prepareStatement(newAnn);
			psAnn.setInt(1, user);
			psAnn.setInt(2, imm);
			psAnn.setString(3, annotazione);
			psAnn.setTimestamp(4, timestamp);
			psAnn.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			psAnn.close();
			cp.freeConnection(conn);
		}
	}
	
	public boolean eliminaCampagna(int idCampagna) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "DELETE FROM Campagna WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			ps.executeUpdate();
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			
			ps.close();
			cp.freeConnection(conn);
		}
		
		return false;
	}

	public List<Immagine> getImmaginiAnnotate(int idCampagna) throws SQLException{
		List<Immagine> immagini = new ArrayList<Immagine>();
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "SELECT * FROM Immagine WHERE idCampagna=? AND nAnnotazioni>0";
			ps = conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			while(rs.next()){
				Immagine imm = new Immagine();
				imm.setIdImmagine(rs.getInt("idImmagine"));
				imm.setIdCampagna(idCampagna);
				imm.setPath(rs.getString("path"));
				immagini.add(imm);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			rs.close();
			ps.close();
			cp.freeConnection(conn);
		}
		return immagini;
	}
	
	public List<Annotazione> getAnnotazioni(int idCampagna) throws SQLException{
		List<Annotazione> annotazioni = new ArrayList<Annotazione>();
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "SELECT * FROM Annotazione WHERE idImmagine IN "
					+ "(SELECT idImmagine FROM Immagine WHERE idCampagna = ? AND nAnnotazioni>0)";
			ps = conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			while(rs.next()){
				Annotazione ann = new Annotazione();
				ann.setIdUtente(rs.getInt("idUtente"));
				ann.setIdImmagine(rs.getInt("idImmagine"));
				ann.setJson(rs.getString("annData"));
				annotazioni.add(ann);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			rs.close();
			ps.close();
			cp.freeConnection(conn);
		}
		return annotazioni;
	}
	
	//public boolean checkGruppoSel(int utente,int idCampagna, Connection conn) throws SQLException{
	private boolean checkGruppoSel(int utente,int idCampagna, Connection conn) throws SQLException{
		//ConnectionPool cp = null;
		//Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean check = false;
		try{
			//cp = ConnectionPool.getInstance();
			//conn = cp.getConnection();
			String query = "SELECT * FROM GruppoSel WHERE idUtente = ? and idCampagna=?";
			ps = conn.prepareStatement(query);
			ps.setInt(1, utente);
			ps.setInt(2, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				check = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		} finally{
			rs.close();
			ps.close();
			//cp.freeConnection(conn);
		}
		return check;
	}
	
	//public boolean checkGruppoAnn(int utente,int idCampagna) throws SQLException{
	private boolean checkGruppoAnn(int utente,int idCampagna, Connection conn) throws SQLException{
		//ConnectionPool cp = null;
		//Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean check = false;
		try{
			//cp = ConnectionPool.getInstance();
			//conn = cp.getConnection();
			String query = "SELECT * FROM GruppoAnn WHERE idUtente = ? and idCampagna=?";
			ps = conn.prepareStatement(query);
			ps.setInt(1, utente);
			ps.setInt(2, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				check = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		} finally{
			rs.close();
			ps.close();
			//cp.freeConnection(conn);
		}
		return check;
	}
	
	public boolean checkConclusa(int idCampagna) throws SQLException{
		ConnectionPool cp = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			String query = "SELECT CONCLUSA FROM CAMPAGNA WHERE IDCAMPAGNA=?";
			ps= conn.prepareStatement(query);
			ps.setInt(1, idCampagna);
			rs = ps.executeQuery();
			if(rs.next()){
				return rs.getBoolean("conclusa");
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			ps.close();
			rs.close();
			cp.freeConnection(conn);
		}
		
		return false;
	}
	
	public List<StatLavoratore> getStatLavoratore(int idUtente) throws SQLException{
		ConnectionPool pool = ConnectionPool.getInstance();
		Connection connection = pool.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String query=null;
		List<StatLavoratore> stat=new ArrayList<StatLavoratore>();
		
		try {
			query = "SELECT * FROM statlavselezione WHERE idUtente= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idUtente);
			rs= stmt.executeQuery();
			while(rs.next()){
				StatLavoratore s=new StatLavoratore();
				s.setIdUtente(idUtente);
				s.setIdCampagna(rs.getInt("idCampagna"));
				s.setnImmApprovate(rs.getInt("immApprovate"));
				s.setnImmRifiutate(rs.getInt("immRifiutate"));
				s.setnSelezioniFatte(rs.getInt("totSelezioniFatte"));
				s.setnSelezioniDaFare(rs.getInt("SelezioniDaFare"));
				stat.add(s);
				
				//System.out.println(s.getnImmApprovate()+" "+s.getnImmRifiutate() );
			} 
			
			
			
			query = "SELECT * FROM statlavannotazione WHERE idUtente= ?";
			stmt= (PreparedStatement) connection.prepareStatement(query);
			stmt.setInt(1, idUtente);
			rs= stmt.executeQuery();
			while(rs.next()){
				int idCampagna=rs.getInt("idCampagna");
				for (int i = 0; i < stat.size(); i++) {
					if(stat.get(i).getIdCampagna()==idCampagna){
						stat.get(i).setnImmDaAnnotare(rs.getInt("nImmDaAnnotare"));
						stat.get(i).setnImmAnnotate(rs.getInt("nImmAnnotate"));
					}
				}
				
			}
			
			
			
			return stat;
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			stmt.close();
			rs.close();
			pool.freeConnection(connection);
		}
		
		return stat;
	}
	
	public StatusLavoratore checkStatusLavoratore(int campagna, int utente) throws SQLException{
		StatusLavoratore status = new StatusLavoratore();
		ConnectionPool cp = null;
		Connection conn = null;
		try{
			cp = ConnectionPool.getInstance();
			conn = cp.getConnection();
			//da aggiungere getImmagine
			status.setSel(checkGruppoSel(utente, campagna, conn) && getImmagineSel(campagna,utente,conn).getPath()!="");
			status.setAnn(checkGruppoAnn(utente, campagna, conn) && getImmagineAnn(campagna,utente,conn).getPath()!="");
			//System.out.println("selezione"+status.getSel());
			//System.out.println("annotazione"+status.getAnn());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			cp.freeConnection(conn);
		}
		return status;
	}
}