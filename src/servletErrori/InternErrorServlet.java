package servletErrori;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/InternErrorServlet")
public class InternErrorServlet extends HttpServlet {
	private static final long serialVersionUID = 8542722276229917801L;

	public InternErrorServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		String err = "Si e' verificato un problema nel processare la tua richiesta.";
		request.setAttribute("errore", err);
		getServletContext().getRequestDispatcher("/ErrorPage.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
