package servletErrori;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ErrorServlet")
public class ErrorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ErrorServlet() {
		super();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {      
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		
		//questa servlet in teoria non dovrebbe essere mai chiamata se c'è un eccezione prevista nel web.xml. in caso contrario finisce qui
		Throwable eccezione = (Throwable) request.getAttribute("javax.servlet.error.exception");
		Integer codice = (Integer) request.getAttribute("javax.servlet.error.status_code");
		String nomeServlet = (String) request.getAttribute("javax.servlet.error.servlet_name");
		if (nomeServlet == null){
			nomeServlet = "Sconosciuto";
		}
		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
		if (requestUri == null){
			requestUri = "Sconosciuto";
		}

		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>\n" + "<html>\n" +"<head><title> Errore </title></head>\n" + "<body>\n");

		if (eccezione == null && codice == null){
			out.println("<h2>Errore Sconosciuto</h2>");
		}else if (codice != null){
			out.println("Codice di errore : " + eccezione);
		}else{
			out.println("<h2>Informazioni sull'errore</h2>");
			out.println("Nome della Servlet : " + nomeServlet + "</br></br>");
			out.println("Tipo di errore : " + eccezione.getClass().getName() + "</br></br>");
			out.println("Richiesta : " + requestUri + "<br><br>");
			out.println("Messaggio : " + eccezione.getMessage());
		}
		//out.println("Ritorna alla <a href=\"" + response.encodeURL("http://localhost:8080/login.jsp") + "\">pagine iniziale</a>.");
		out.println("Ritorna alla <a href='login.jsp'>pagine iniziale</a>.");
		out.println("</body>");
		out.println("</html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
