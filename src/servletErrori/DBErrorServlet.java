package servletErrori;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/DBErrorServlet")
public class DBErrorServlet extends HttpServlet {
	private static final long serialVersionUID = -8582824873585494421L;
	
	public DBErrorServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		//setto sempre l'attributo errore e ErrorPage è standard.
		String err = "Si e' verificato un problema durante l'accesso al database.";
		request.setAttribute("errore", err);
		getServletContext().getRequestDispatcher("/ErrorPage.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
