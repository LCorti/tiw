package servletLavoratore;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Immagine;

//import com.sun.org.apache.xml.internal.security.utils.Base64;

import database.DBManager;

@WebServlet("/RecuperaImmaginiServlet")
public class RecuperaImmaginiServlet extends HttpServlet {
	private static final long serialVersionUID = 5425656679855539529L;
	
	private DBManager db = null;
	
	public RecuperaImmaginiServlet() {
        super();
    }
	
	public void init(){
		db = new DBManager();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		int idCamp = Integer.parseInt(request.getParameter("idCamp"));
		int idUsr = Integer.parseInt(request.getParameter("idUsr"));
		String type = request.getParameter("type");
		Immagine img = new Immagine();
		String path = request.getSession().getServletContext().getRealPath("");
		try {
			PrintWriter out = response.getWriter();
			//System.out.println(type);
			//il metodi getImmagine hanno 3 paramentri perchè vengono usati anche nella chiamata ajax. il null sta per la connessione.
			if(type.equals("sel")){
				img = db.getImmagineSel(idCamp, idUsr, null);
			}else if(type.equals("ann")){
				img = db.getImmagineAnn(idCamp, idUsr, null);
			}
			//System.out.println("immagine presa");
			if(img.getPath()!=""){
				out.print("/"+img.getPath());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
