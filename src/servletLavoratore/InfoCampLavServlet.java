package servletLavoratore;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Immagine;
import checks.StatusLavoratore;
import database.DBManager;

@WebServlet("/InfoCampLavServlet")
public class InfoCampLavServlet extends HttpServlet {
	private static final long serialVersionUID = -5394801023829140033L;
	
	private DBManager db = null;

	public InfoCampLavServlet() {
        super();
    }

	public void init(){
		//db = DBManager.getInstance();
		db = new DBManager();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		int idCamp = Integer.parseInt(request.getParameter("idCamp"));
		int idUsr = Integer.parseInt(request.getParameter("usr"));
		PrintWriter out = response.getWriter();
		String dataXHR = "<br><br><div id='tasks'>";
		StatusLavoratore sl = new StatusLavoratore();
		try {
			sl = db.checkStatusLavoratore(idCamp, idUsr);
			if(sl.getSel()){
				dataXHR+="Selezione delle immagini in sospeso: "
						+ "<form method='get' action='selezioneImmagini.jsp'>"
						+ "<input type='hidden' name='idC' value ='"+idCamp+"'>"
						+ "<input type='submit' value='Avvia Selezione'></form><br>";
			}
			if(sl.getAnn()){
				dataXHR+="Annotazione delle immagini in sospeso: "
						+ "<form method='get' action='annotazioneImmagini.jsp'>"
						+ "<input type='hidden' name='idC' value='"+idCamp+"'>"
						+ "<input type='submit' value='Avvia Annotazione'></form>";
			}
			if(!sl.getSel() && !sl.getAnn()){
				dataXHR+="Per questa campagna hai gia' svolto tutti i task che ti erano stati assegnati. "
						+ "Prova con un'altra.";
			}
			/*
			if(db.checkGruppoSel(idUsr,idCamp)){
				img = db.getImmagineSel(idCamp, idUsr);
				if(img.getPath()!=""){
					dataXHR+="Selezione delle immagini in sospeso: "
							+ "<form method='get' action='selezioneImmagini.jsp'>"
							+ "<input type='hidden' name='idC' value ='"+idCamp+"'>"
							+ "<input type='submit' value='Avvia Selezione'></form><br>";
				}
			}
			if(db.checkGruppoAnn(idUsr,idCamp)){
				img = db.getImmagineAnn(idCamp, idUsr);
				if(img.getPath()!=""){
					dataXHR+="Annotazione delle immagini in sospeso: "
							+ "<form method='get' action='annotazioneImmagini.jsp'>"
							+ "<input type='hidden' name='idC' value='"+idCamp+"'>"
							+ "<input type='submit' value='Avvia Annotazione'></form>";
				}
			}*/
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dataXHR+="<br></div>";
			out.print(dataXHR);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
