package servletLavoratore;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBManager;

@WebServlet("/NuovaAnnotazioneServlet")
public class NuovaAnnotazioneServlet extends HttpServlet {
	private static final long serialVersionUID = 9064436957903409857L;
	
	private DBManager db = null;
	
	public NuovaAnnotazioneServlet() {
        super();
    }

	public void init(){
		db = new DBManager();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		int utente = Integer.parseInt(request.getParameter("idU"));
		int immagine = Integer.parseInt(request.getParameter("idImm"));
		String jsonString=request.getParameter("coordinates");
		//System.out.println("Stringa JSON: "+jsonString);
		try{
			db.salvaAnnotazione(utente, immagine, jsonString);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
