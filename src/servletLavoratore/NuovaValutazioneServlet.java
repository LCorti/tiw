package servletLavoratore;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DBManager;

@WebServlet("/NuovaValutazioneServlet")
public class NuovaValutazioneServlet extends HttpServlet {
	private static final long serialVersionUID = -435244458506098249L;
	
	private DBManager db = null;

	public void init(){
		db = new DBManager();
	}
	
    public NuovaValutazioneServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		int idImmagine = Integer.parseInt(request.getParameter("immagine"));
		int idUtente = Integer.parseInt(request.getParameter("utente"));
		int esito = Integer.parseInt(request.getParameter("voto"));
		try{
			db.salvaEsito(idImmagine,idUtente,esito);
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
