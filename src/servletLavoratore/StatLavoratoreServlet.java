package servletLavoratore;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.StatLavoratore;
import database.DBManager;

/**
 * Servlet implementation class StatLavoratore
 */
@WebServlet("/StatLavoratoreSevlet")
public class StatLavoratoreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private DBManager db = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatLavoratoreServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init(){
    	db = new DBManager();
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String referer = request.getHeader("Referer");
		if(referer==null){
			response.sendRedirect("DirectAccessServlet");
			return;
		}
		
		int idUtente=Integer.parseInt(request.getParameter("idUtente"));
		List<StatLavoratore> stat=new ArrayList<StatLavoratore>();
		System.out.println("id passato" + idUtente);
		
		
		try {
			stat=db.getStatLavoratore(idUtente);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("idUtente", idUtente);
		request.setAttribute("statistiche", stat);
		getServletContext().getRequestDispatcher("/StatLavoratorePerCampagna.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
