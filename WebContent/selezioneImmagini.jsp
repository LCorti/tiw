<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="beans.Utente" %>

<%
	HttpSession usrSession = request.getSession(false);
	Utente user = (Utente) usrSession.getAttribute("user");
	int c = Integer.parseInt(request.getParameter("idC"));
	if(user.equals(null) || !"lavoratore".equals(user.getRuolo())){
		response.sendRedirect("login.jsp");
	} else {
%>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Selezione Immagini</title>
	<link rel="stylesheet" href="stylesheets/w3.css">
	<script type="text/javascript" src="scriptJS/jQuery.js"></script>
	<script>
		var photoId;
		var photoData;
		var recuperaImmaginiSel = function (idC, idU){
			$('#fineSel').hide();
			$('#scelta').show();
			$.ajax({
				url:'RecuperaImmaginiServlet',
				data:{idCamp: idC, idUsr: idU, type: "sel"},
				type:'get',
				success: function(result){
					if(result!=""){
						photoId = result.substring(16,result.lastIndexOf('.'));
						$("[name='foto']").attr("src", "${pageContext.request.contextPath}"+result);
					} else {
						alert("Hai completato il task di selezione per questa campagna, clicca su 'Esci' per tornare alla tua homepage.");
						$('#fineSel').show();
						$('#scelta').hide();
					}
				},
				error:function(){
					alert('Errore, riprova');
				}
			});
		}
		
		var comunicaVoto = function(c, u, esito){
			
			$.ajax({
				url:'NuovaValutazioneServlet',
				data:{immagine: photoId, utente:u, voto: esito},
				type:'post',
				success: function(data){
					alert("Voto comunicato, attendi che la prossima immagine venga caricata");
				},
				complete: function(data){
					recuperaImmaginiSel(c, u);
				},
				error:function(){
					alert('Errore, riprova');
				}
			});
		}
	</script>
</head>
<body onload='recuperaImmaginiSel(<%=c%>,<%= user.getIdUtente()%>)'  background="${pageContext.request.contextPath}/bgImage/login.jpg">
	<div class="w3-display-container" style="width:90%; margin:0 auto;">
		<div id='container'>
			<div id='scelta'>
				<input type='button' name='ok' value='Approva' onclick='comunicaVoto(<%= c%>,<%=user.getIdUtente()%>,1)'>
				<input type='button' name='no' value='Rifiuta' onclick='comunicaVoto(<%= c%>,<%=user.getIdUtente()%>,0)'>
			</div>
			<div id='divPhoto'>
				<img name='foto' src=''>
			</div>
		</div>
		<div id='fineSel'>
			<form method='GET' action='HomeLavoratore.jsp'>
				<input type='submit' value='Esci'/>
			</form>
		</div>
	</div>
</body>
</html>
<% } %>