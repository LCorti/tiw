<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="beans.Utente" %>
<%@ page import="beans.Campagna" %>
<%@ page import="java.util.List" %>

<%	
	int c = Integer.parseInt(request.getParameter("idC"));
	HttpSession usrSession = request.getSession(false);
	Utente user = (Utente) usrSession.getAttribute("user");
	List<Campagna> camp;
	int dimL=1;
	if(user==null || !"lavoratore".equals(user.getRuolo())){
		response.sendRedirect("login.jsp");
	} else {
		camp = (List<Campagna>) usrSession.getAttribute("campagne");
		for(int i=0; i<camp.size(); i++){
			if(c==camp.get(i).getIdCampagna()){
				dimL = camp.get(i).getDimLineaAnnotazione();
			}
		}
%>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="stylesheets/w3.css">
	<script type="text/javascript" src="scriptJS/jQuery.js"></script>
	<script type="text/javascript" src="scriptJS/drawScript.js"></script>
	<script type="text/javascript">
        $(document).ready(function() {
        	recuperaImmaginiAnn(<%= c %>,<%= user.getIdUtente() %>);
        });
        
        var photoId;
        
        var recuperaImmaginiAnn = function(idC, idU){
        	$('#fineAnn').hide();   	
        	$('#tools').show();   	
        	$.ajax({
        		url:'RecuperaImmaginiServlet',
        		data:{idCamp: idC, idUsr: idU, type: "ann"},
        		type:'get',
        		success: function(result){
        			if(result!=""){
        				photoId = result.substring(16,result.lastIndexOf('.'));
        				
        				//<img id="container" style="position: absolute; z-index: 0;"/>
        				var immagine = document.createElement("img");
        				immagine.id = "container";
        				immagine.src = "${pageContext.request.contextPath}"+result;
        				immagine.style.position = "absolute";
        				immagine.style.zIndex = "0";
        				$(immagine).on('load', function(){
        					prepareSimpleSizesCanvas(<%= dimL%>);
        				});
        				
        				if(document.getElementById('container')){
        					document.getElementById('container').remove();
        				}
        				
        				$(immagine).insertBefore($('#canvasSimpleSizesDiv'));
        				
        			} else {
        				alert("Hai completato il task di annotazione per questa campagna, clicca su 'Esci' per tornare alla tua homepage.");
        				$('#fineAnn').show();
        				$('#tools').hide();
        			}
        		}
        	});
        }
        
    	var inviaAnnotazione = function(idUsr){
    		var coordinatesArray = {"tratto": <%=dimL%>, "coordinateX" : clickX_simpleSizes,"coordinateY": clickY_simpleSizes};
    		var jsonAnnotation= JSON.stringify(coordinatesArray);

    		$.ajax({
    			url:'NuovaAnnotazioneServlet',
    			data:{coordinates: jsonAnnotation, idU: idUsr, idImm: photoId},
    			type:'post',
    			cache:false,
    			success:function(data){
    				//alert(data);
    				//$('#somediv').text(responseText); 
    				alert("Annotazione registrata, attendi che la prossima immagine venga caricata.");
    			},
    			complete: function(result){
    				recuperaImmaginiAnn(<%= c %>,<%= user.getIdUtente() %>);
    			},
    			error:function(){
    				alert('error');
    			}
    		});		
    	}
        
    </script>
<title>Annotazione Immagini</title>
</head>
<body background="${pageContext.request.contextPath}/bgImage/login.jpg">
	<div class="divAlpha">
	<div style="width:100%; height:100%; margin-right:auto; margin-left:auto;">
    <div id="tools">
    <p class="demoToolList">
            Rimuovi annotazione: <button id="clearCanvasSimpleSizes" type="button">Pulisci</button>
            <button id="send" type="button" onclick="inviaAnnotazione(<%=user.getIdUtente()%>)">Registra Annotazione</button>
    </p>
    </div>
        <div class="image" style="width:auto; height:auto; background-repeat: no-repeat;">
            <div id="canvasSimpleSizesDiv" style="width:100%; height: 100%; position: relative; z-index: 1;"></div>
        </div>
    </div>
    <div id='fineAnn'>
		<form method='GET' action='HomeLavoratore.jsp'>
			<input type='submit' value='Esci'/>
		</form>
	</div>
	</div>	
</body>
</html>

<% }%>