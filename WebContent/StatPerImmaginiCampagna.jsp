<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>   
<%@ page import="java.util.List" %>
<%@ page import="database.DBManager" %>
<%@ page import="beans.Utente" %>
<%@ page import="beans.Immagine" %>
<%@ page import="beans.StatCampagna" %>

<%

HttpSession s = request.getSession(true);
Utente u = (Utente) s.getAttribute("user");
if (u== null || u.getIdUtente()==0||request.getParameter("idCampagna")==null || !"gestore".equals(u.getRuolo())) {
		response.sendRedirect("login.jsp");
}	
else{
%>    
    
    
<!DOCTYPE html>
<html>
	<head>
		<%
			int idCampagna=Integer.parseInt(request.getParameter("idCampagna"));
			List<Immagine> immagini= (List<Immagine>) request.getAttribute("immagini");
			StatCampagna stat= (StatCampagna)request.getAttribute("statistiche");
				
		%>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Statistiche Campagna <%=idCampagna %> </title>
		<link rel="stylesheet" href="stylesheets/overlayPhotoStat.css">
		<link rel="stylesheet" href="stylesheets/w3.css">
		<script type="text/javascript" src="scriptJS/loader.js"></script>
		<script>
		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawBasic);
		//alert("sono")
		function drawBasic() {
			
			var data = google.visualization.arrayToDataTable([
			        ['Genre','Immagini Approvate', 'Immagini Rifiutate', 'Immagini Annotate', { role: 'annotation' } ],
			        ['Task Selezione', <%=stat.getnImmaginiApprovate() %>, <%=stat.getnImmaginiRifiutate()%>, 0, ''],
			        ['Task Annotazione', 0, 0, <%=stat.getnImmaginiAnnotate() %>, '']
			      ]);

			      var options = {
			    		  title: 'Avanzamento dei Task dela tua campagna',
			    	        
			    		  isStacked: true,
			              height: 300,
			              legend: {position: 'top', maxLines: 3},
			              vAxis: {minValue: 0}
			      };

			      var chart = new google.visualization.ColumnChart(
			        document.getElementById('chart_div'));

			      chart.draw(data, options);
		}
		
		
		
		</script>
	</head>
	<body  background="${pageContext.request.contextPath}/bgImage/login.jpg" >
	<div class="w3-display-container" style="width:90%; margin:0 auto;">
		<header>
			<h1>
				Statistiche accurate per la tua Campanga!
			</h1>
			<h3>
				Statistiche complessive della campagna numero <%=idCampagna %>
			</h3>
		</header>
		<br>
		<div id="chart_div" style="max-width:60%"></div>
		<br>		
		<div style="position:relative">
			Colonna "task selezione": indica il numero di Immagini Approvate e Immagini Rifiutate quando hanno completato il task di selezone.
			<br>
			<br>
			Colonna "task Annotazione": indica il numero di immagini Annotate che hanno superato positivamente il task di selezione.
		</div>
		<br>
		<form action='HomeGestore.jsp' >
			<input type="submit" value="torna alla home" />
		</form>
		<hr>
		<h2>
			Statistiche per singola immagine	
		</h2>
		<h3>
			Passa con il mouse sopra l'immagine per scoprire le statistiche.
		</h3>
		<div style="height:1000px;"> &nbsp;
		<%
			for(int i=0;i<immagini.size();i++){
				System.out.print(immagini.get(i).getPath());
		%>
		
			
		    <div class="container" align="left">
			  <img src="${pageContext.request.contextPath}/<%=immagini.get(i).getPath()%>" alt="Avatar" class="image">
			  <div class="overlay">
			    <div class="text">
			    
			    Numero selezioni positive: <%=immagini.get(i).getnOk() %><br>
			    Numero selezioni negative: <%=immagini.get(i).getnNo() %><br>
			   	Task di selezione terminato: <%=immagini.get(i).isSelezioneFinita()%><br><br>
			    <!-- Ha raggiunto N sel OK: <%=immagini.get(i).isAnnotabile() %><br> -->
			    
			    <% 
				if(immagini.get(i).isAnnotabile()){
					%>
				    Numero annotazioni: <%=immagini.get(i).getnAnnotazioni() %><br>
				    Task di Annotazione completato: <%=immagini.get(i).isAnnotazioneFinita() %><br>
				<% 
				}else {
					if(immagini.get(i).isSelezioneFinita()){
				%>	
					<br>L'immagine NON ha superato <br>la fase di selezione.
				<%
					}else{
						%>	
							L'immagine é ancora al task selezione.
						<% 
					}
				}
				%>	  
			    </div>
			  </div>
			</div>
		    
		    
	   
		<% 
			}
		%>	
		<br>
		<br>
		<div>
		<form action='HomeGestore.jsp' >
			<input type="submit" value="torna alla home" />
		</form>
		</div>
	</div> 
		
	</div>
	</body>
</html>
<% 
	}
%>	