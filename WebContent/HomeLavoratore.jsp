<%@ page import="beans.Utente" %>
<%@ page import="beans.Campagna"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
	HttpSession userSession = request.getSession(false);
	Utente user = (Utente) userSession.getAttribute("user");
	List<Campagna> camp = null;
	if(user== null || user.getIdUtente()==0 || !"lavoratore".equals(user.getRuolo())){
		response.sendRedirect("login.jsp");
	} else {
		camp = (List<Campagna>) session.getAttribute("campagne");
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="stylesheets/w3.css">
	<title>Home Lavoratore</title>
	<script type="text/javascript" src="scriptJS/jQuery.js"></script>
	<script>
	function mostraInfo(idCampagna, idUsr){
		$.ajax({
			url:'InfoCampLavServlet',
			data:{idCamp: idCampagna, usr: idUsr},
			type:'post',
			cache:false,
			success:function(data){
				//alert(data);
				$('#infoCampagna').html(data);
			},
			error:function(){
				alert('error');
			} 
		});
	};
	</script>
</head>
<body background="${pageContext.request.contextPath}/bgImage/login.jpg">
<div class="w3-display-container" style="width:90%; margin:0 auto;">
	<div id="datiUsr">
	<h1>Benvenuto <%= user.getNome() %> <%= user.getCognome() %></h1>
	<form class="w3-display-topright" style="right:10px;" action="LogoutServlet">
    		<input type="submit" value="Logout" />
	</form>
	<hr>
	Consulta le tue statistiche per ogni campagna!
	<form action="StatLavoratoreSevlet">
    		<input type="submit" value="Statistiche lavoratore" />
    		<input type="hidden" name="idUtente" value="<%= user.getIdUtente()%>"/>
	</form>
	<hr>
	</div>
	<div id="campLav">
		<table>
			<tr>
				<td>Titolo Campagna</td>
				<td>Stato</td>
				<td>Task da eseguire</td>
			</tr>
			<%
			if(camp.size()!=0){
				for(int i=0;i<camp.size();i++){ %>
				<tr>
					<td><%=camp.get(i).getTitolo() %></td>
					<td>
						<% if(!camp.get(i).isEditabile()){ %>
							Premi a destra per eseguire i task
						<% } else if(camp.get(i).isConclusa()){ %>
							Conclusa
						<% } %>
					</td>
					<td><input type="button" id="extra" value="Mostra" onclick="mostraInfo(<%= camp.get(i).getIdCampagna() %>, <%= user.getIdUtente() %>)"/></td>
				</tr>
			<% }
			} else {
				%> <tr><td></td><td>Non hai campagne su cui lavorare, torna in un altro momento</td><td></td></tr> <%
			}%>
		</table>
	</div>
	
	<div id="infoCampagna">
	</div>
</div>
</body>
</html>
<% } %>