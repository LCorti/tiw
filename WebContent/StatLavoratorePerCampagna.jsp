<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="beans.StatLavoratore" %>
<%@ page import="java.util.List" %>
<%@ page import="database.DBManager" %>
<%@ page import="beans.Utente" %>

<%

HttpSession s = request.getSession(true);
Utente u = (Utente) s.getAttribute("user");
if (u== null || u.getIdUtente()==0||request.getParameter("idUtente")==null || !"lavoratore".equals(u.getRuolo())) {
		response.sendRedirect("login.jsp");
}	
else{
%>


<!DOCTYPE html>
<html>
<head>
	<%
		int idUtente=Integer.parseInt(request.getParameter("idUtente"));
		
		List<StatLavoratore> stat= (List<StatLavoratore>)request.getAttribute("statistiche");
			
	%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Statistiche Lavoratore</title>
<script type="text/javascript" src="scriptJS/loader.js"></script>
<link rel="stylesheet" href="stylesheets/w3.css">
    <script type="text/javascript">
    <%if(stat.size()>0){%>
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        var data = google.visualization.arrayToDataTable([
        	 ['id Campagna','Immagini Annotate', 'Immagini da annotare', 'immagini Approvate', 'immagini Rifiutate', 'Totale selezioni fatte', 'Totale selezioni da fare'],
        <%
			//System.out.println("dimaaaaaa" + stat.size());
	    	for(int i=0;i<stat.size();i++){
	    	
	    %>
	    	['n° <%=stat.get(i).getIdCampagna()%>', <%=stat.get(i).getnImmAnnotate()%>,<%=stat.get(i).getnImmDaAnnotare()%>, <%=stat.get(i).getnImmApprovate()%> ,<%=stat.get(i).getnImmRifiutate()%>,<%=stat.get(i).getnSelezioniFatte()%>,<%=stat.get(i).getnSelezioniDaFare()%>]
	    	
	    		<%if(i< (stat.size()-1)){ %>,<%}

	    	}
		%>
		  ]);
    var options = {
      title : 'Statistiche per singola campagna',
      vAxis: {title: 'numero'},
      hAxis: {title: 'idCampagna'},
      seriesType: 'bars',
      series: {8: {type: 'line'}}
    };

    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
    <%}else{%>
    	alert("non sei ancora stato assegnato a nessuna campagna...");
    <%}%>
    </script>
</head>
<body  background="${pageContext.request.contextPath}/bgImage/login.jpg">
<div class="w3-display-container" style="width:90%; margin:0 auto;">
	<div id="chart_div" style="width: 900px; height: 500px;"></div>
	<form action="HomeLavoratore.jsp">
    		<input type="submit" value="torna alla home" />
	</form>
</div>
</body>
</html>
<% 
	}
%>