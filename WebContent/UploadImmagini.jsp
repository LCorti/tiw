<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="beans.Utente" %>

<%
	int c = Integer.parseInt(request.getParameter("idCampagna"));
	HttpSession usrSession = request.getSession(false);
	Utente user = (Utente) usrSession.getAttribute("user");
	if(user==null||!"gestore".equals(user.getRuolo())){
		response.sendRedirect("login.jsp");
	} else {
%>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Caricamento immagini</title>
	<link rel="stylesheet" href="stylesheets/w3.css">
	<script type="text/javascript" src="scriptJS/jQuery.js"></script>
	<script>
	//var files = document.getElementById('filesUp');
	var aggiornaLista = function(){
		var elem = document.getElementById('filesUp');
		var lista = [];
		for(var i=0; i<elem.files.length; i++){
			lista.push(elem.files[i].name);
		}
		document.getElementById('fileList').innerHTML = lista.join('<br>');
	}
	</script>
</head>
<body  background="${pageContext.request.contextPath}/bgImage/login.jpg">
<div class="w3-display-container" style="width:90%; margin:0 auto;">
	<div id="titolo"><h1>Scegli le foto che vuoi usare per la tua campagna</h1></div>
	<form action="UploadServlet" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="idCampagna" value="<%= c %>">
        <input name="file" id="filesUp" type="file" multiple onchange='aggiornaLista()'></input>
        <input type="submit" value="Carica"/>
    </form>
    <div id="lista"> Lista dei file che verranno caricati</div>
    <div id="fileList"></div>
</div>
</body>
</html>
<% } %>