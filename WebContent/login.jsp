<%@ page import="beans.Utente"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<jsp:useBean id="user" class="beans.Utente" scope="session"/>

<%
	String errore = request.getParameter("err");
	HttpSession usrSession = request.getSession(false);
	user = (Utente) usrSession.getAttribute("user");
	if (user != null) {
		if ("gestore".equals(user.getRuolo())) {
			response.sendRedirect("HomeGestore.jsp");
		} else if ("lavoratore".equals(user.getRuolo())) {
			response.sendRedirect("HomeLavoratore.jsp");
		}
	}
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="stylesheets/w3.css">
	<title>Pagina Iniziale</title>
	<script>
		var caricaLog = function() {
			document.getElementById("reg").style.display = "none";
			document.getElementById("log").style.display = "block";
		}
		var caricaReg = function() {
			document.getElementById("reg").style.display = "block";
			document.getElementById("log").style.display = "none";
		}
	</script>
</head>
<body onload="caricaLog()">
	<img src="${pageContext.request.contextPath}/bgImage/login.jpg" style="width:100%;z-index: -1;overflow: hidden;position: relative;">
	<div class="w3-card-2" style="position:absolute;top:20%;left:50%;margin-top:-100px;margin-left:-250px;width:500px;background:white;border-radius: 5px;">
		
			<div id="errore">
				<% if ("auth".equals(errore)) { %>
				I dati inseriti non corrispondono a nessun utente registrato, ricontrollali o procedi alla fase di registrazione.
				<% } else if ("reg".equals(errore)) { %>
				Lo username che hai scelto corrisponde ad un utente già esistente.
				<% } else if ("exc".equals(errore)) { %>
				Si e' verificato un errore, riprova.
				<% } %>
			</div>
			<div id="log">
				<div class="w3-container w3-green" style="border-radius: 10px;">
				
					<h1>Accedi al portale</h1>
				</div>
				<br>
				<form method="POST" action="LoginServlet" class="w3-container">
					Username: <input type="text" name="usr" class="w3-input" required><br>
					Password: <input type="password" name="pwd" class="w3-input" required><br>
					<input type="submit" value="Accedi">
				</form>
				<br><br>
				<div class="change" onclick="caricaReg()">Sei un nuovo utente? Registrati qui</div>
			</div>
			<div id="reg">
				<div class="w3-container w3-green">
					<h1>Benvenuto, inserisci i tuoi dati</h1>
				</div>
				<form method="POST" action="RegServlet" class="w3-container">
					Nome: <input type="text" name="nome" class="w3-input" required><br> 
					Cognome: <input type="text" name="cognome" class="w3-input" required><br> 
					Username: <input type="text" name="username" class="w3-input" required><br> 
					Password: <input type="password" name="password" class="w3-input" required><br><br>
					Ruolo: <select name="ruolo">
						<option value="gestore">Gestore</option>
						<option value="lavoratore">Lavoratore</option>
					</select><br><br><br>
					<input type="submit" value="Registrati"><br>
				</form><br>
				<div class="change" onclick="caricaLog()">Annulla</div>
			</div>
		
	</div>
</body>
</html>