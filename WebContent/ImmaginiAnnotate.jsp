<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="beans.Utente" %>
<%@ page import="beans.Immagine" %>
<%@ page import="beans.Annotazione" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
    
<%
	HttpSession usrSession = request.getSession(false);
	Utente user = (Utente) usrSession.getAttribute("user");
	if(user==null||!"gestore".equals(user.getRuolo())){
		response.sendRedirect("login.jsp");
	} else {
		List<Immagine> foto = (ArrayList<Immagine>) request.getAttribute("immaginiComplete");
		List<Annotazione> ann = (ArrayList<Annotazione>) request.getAttribute("annotazioniComplete");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="stylesheets/w3.css">
	<script type="text/javascript" src="scriptJS/drawScript.js"></script>
	<script type="text/javascript" src="scriptJS/jQuery.js"></script>
	<script>
		var indice_ann = 0;
		var annotazioni = [];
		var annotazioniImmagine = [];
		
		//prendo il mio array di annotazioni in java a lo tramuto il js. ogni pezzetto java corrisponde a un oggetto con idImm e data(=coordinate)
		<% 
		for(int i=0; i<ann.size(); i++){
			%>
			annotazioni.push({idImm:<%=ann.get(i).getIdImmagine()%>, data:<%=ann.get(i).getJson()%>});
			<%
		}
		%>
		
		var disegnaCanvas = function(val){
			cambiaAnnotazione(val);
			$('w3-display-container').css('left', 'auto');
			$('w3-display-container').css('top', 'auto');
			prepareSimpleSizesCanvas(annotazioniImmagine[indice_ann].tratto);
			updateXY(annotazioniImmagine[indice_ann].tratto, annotazioniImmagine[indice_ann].coordinateX, annotazioniImmagine[indice_ann].coordinateY);
		}
		
		var caricaFoto = function(idFoto){
			var cliccato = document.getElementById(idFoto);
			
			
			var immagine = document.createElement("img");
			immagine.id = "container";
			immagine.src = cliccato.getAttribute("src");
			immagine.style.position = "absolute";
			immagine.style.zIndex = "0";
			$(immagine).on('load',function(){
				disegnaCanvas(0);
			});
			if(document.getElementById('container')){
				document.getElementById('container').remove();
			}
			$(immagine).insertBefore($('#canvasSimpleSizesDiv'));
			
			//svuoto array prima di aggiungere nuove annotazioni
			annotazioniImmagine = [];
			for(var j=0; j<annotazioni.length; j++){
				if(idFoto==annotazioni[j].idImm){
					annotazioniImmagine.push(annotazioni[j].data);
				}
			}
			indice_ann=0;
		}
		var cambiaAnnotazione = function(val){
			indice_ann = indice_ann + val;
			if(indice_ann<0){
				indice_ann=annotazioniImmagine.length;
			}
			if(indice_ann>=annotazioniImmagine.length){
				indice_ann=0;
			}
		}
		
	</script>
	<title>Immagini Annotate</title>
</head>
<body background="${pageContext.request.contextPath}/bgImage/login.jpg">
	<div id="divAlpha">
	<table id='annotazioni'>
		<tr><td style="max-width: 400px;">
			<table id='miniature' style="border: solid;">
				<%for(int i=0; i<foto.size(); i++){ %>
				<tr><td>
					<img id="<%= foto.get(i).getIdImmagine() %>" src="${pageContext.request.contextPath}/<%=foto.get(i).getPath()%>" 
						style="width:100%; height: auto;"
						onclick="caricaFoto(<%= foto.get(i).getIdImmagine()%>)">
				</td></tr>
				<%} %>
			</table>
		</td>
		<td style="position:fixed;">
			<div id='foto' style="width: auto; height: auto; background-repeat: no-repeat; position:absolute; left:0px; top:0px;">
				<div id="canvasSimpleSizesDiv" style="pointer-events: none; width:100%; height: 100%; position: relative; z-index: 1;"></div>
				<div class="w3-display-container">
     				<p id="caption"></p>
     				<span class="w3-display-left w3-btn" onclick="disegnaCanvas(-1)">❮</span>
     				<span class="w3-display-right w3-btn" onclick="disegnaCanvas(1)">❯</span>
    			</div>
    			<br><hr>
    			<form method='get' action='HomeGestore.jsp'>
    				<input type='submit' value='Torna alla tua Home'/>
    			</form>
			</div>
		</td></tr>
	</table>
	</div>
</body>
</html>
<%} %>