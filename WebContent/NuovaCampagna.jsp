<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ page import="beans.Utente" %>
<%

HttpSession s = request.getSession(true);
Utente u = (Utente) s.getAttribute("user");
if (u== null || u.getIdUtente()==0 || !"gestore".equals(u.getRuolo())) {
		response.sendRedirect("login.jsp");
}	
else{
%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Crea nuova campagna</title>
	<script type="text/javascript" src="scriptJS/jQuery.js"></script>
    <script type="text/javascript" src="scriptJS/drawScript.js"></script>
    <link rel="stylesheet" href="stylesheets/w3.css">
</head>
<body background="${pageContext.request.contextPath}/bgImage/login.jpg">
<div class="divAlpha">
	<h1>Nuova Campagna</h1>
	<form action="RegistrazioneCampagna" method="post" onsubmit="return validateForm()" name="myForm">
		Titolo: <input name="titolo" placeholder="titolo della campagna" required><br><br>
		Parametro N: <input name="parN" type="number" min="1" max="10000" step="1" value="0" required pattern="^[0-9]*$"><br>
		Parametro K: <input name="parK" type="number" min="1" max="10000" step="1" value="0" required pattern="^[0-9]*$"><br>
		Parametro M: <input name="parM" type="number" min="1" max="10000" step="1" value="0" required pattern="^[0-9]*$"><br><br>
		Dimensione riga Annotazione: minimo <input id="radius" name="dimLineaAnnotazione" type="range" min="1" max="10" step="1" value="1" onchange="aggiornaDim(this.value)"  required> massimo<br>
		<br><br>
		<input type="submit" value="CREA">
	</form>
	<br><br>
	prova a disegnare:
	<div id="test" style="border-style: groove; height:100px; width:300px; background-color:white;">
		<div id ="container" style="height:100px; width:300px; position:absolute;"></div>
		<div id="canvasSimpleSizesDiv" style="position:relative;" ></div>
	</div>
	
	
	<br><br>
</div>
	<script type="text/javascript"> 
        $(document).ready(function() {
        	//aggiornaDim(5);$('#radius').val
        	
            prepareSimpleSizesCanvas(document.getElementById("radius").value);
        });
        
        function validateForm(){
        	//VALIDO TITOLO
        	var t = document.forms["myForm"]["titolo"].value;
		    if (t == "") {
		        alert("Devi dare un titolo alla tua campagna.");
		        return false;
		    }
        	
        	
        	//VALIDO PARN
        	var n = parseInt(document.forms["myForm"]["parN"].value);
        	//alert("parn" + n)
            if (isNaN(n) || n < 1 || n > 100) {
                alert("Il valore che hai inserito per parametro N non è valido.");
                return false;
            }
            
          //VALIDO PARk
        	var k = parseInt(document.forms["myForm"]["parK"].value);
        	//alert("park" + k)
            if (isNaN(k) || k < 1) {
                alert("Il valore che hai inserito per parametro K non è valido.");
                return false;
            }
            else{ 
            	if(k>n){
	            	alert("Il parametro K deve essere inferiore (o uguale) al parametro N.");
	            	return false;
            	}
            }            
            
            
          //VALIDO PARm
        	var m = parseInt(document.forms["myForm"]["parN"].value);
            if (isNaN(m) || m < 1 || m > 100) {
                alert("Il valore che hai inserito per parametro M non è valido.");
                return false;
            }
            
            
        }
    </script>
</body>
</html>
<%}%>