<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String err = request.getAttribute("errore").toString();
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="stylesheets/w3.css">
<title></title>
</head>
<body background="${pageContext.request.contextPath}/bgImage/login.jpg">
	<div class="w3-display-container">
		<%= err %><br>
		<a href='login.jsp'>Ritorna alla tua pagina iniziale.</a>
	</div>
</body>
</html>