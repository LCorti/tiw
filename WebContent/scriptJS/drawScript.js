var colorPurple = "#cb3594";
var colorGreen = "#659b41";
var colorYellow = "#ffcf33";
var colorBrown = "#986928";

var canvasWidth;
var canvasHeight;
var clickX_simpleSizes = new Array();
var clickY_simpleSizes = new Array();
var clickDrag_simpleSizes = new Array();
var clickColor_simpleSizes = new Array();
var clickSize_simpleSizes = new Array();
var paint_simpleSizes;
var canvas_simpleSizes;
var context_simpleSizes;
var curColor_simpleSizes = colorPurple;
var curSize_simpleSizes = "normal";
var dim;



function prepareSimpleSizesCanvas(dimLinea)
{	
	if(document.getElementById('canvasSimpleSizesDiv').hasChildNodes()){
		//alert(document.getElementById('canvasSimpleSizesDiv').childNodes[0].nodeName);
		document.getElementById('canvasSimpleSizes').remove();
		aggiornaDim(dim);
	}
	dim=dimLinea;
	
	// Create the canvas (Necessary for IE because it doesn't know what a canvas element is)
	canvasWidth = document.getElementById('container').clientWidth;
	canvasHeight = document.getElementById('container').clientHeight;

	var canvasDiv = document.getElementById('canvasSimpleSizesDiv');
	canvas_simpleSizes = document.createElement('canvas');
	canvas_simpleSizes.setAttribute('width', canvasWidth);
	canvas_simpleSizes.setAttribute('height', canvasHeight);
	canvas_simpleSizes.setAttribute('id', 'canvasSimpleSizes');
	
	canvas_simpleSizes.style.cursor="crosshair";
	
	canvasDiv.appendChild(canvas_simpleSizes);
	if(typeof G_vmlCanvasManager != 'undefined') {
		canvas_simpleSizes = G_vmlCanvasManager.initElement(canvas_simpleSizes);
	}
	context_simpleSizes = canvas_simpleSizes.getContext("2d"); // Grab the 2d canvas context
	// Note: The above code is a workaround for IE 8 and lower. Otherwise we could have used:
	//     context = document.getElementById('canvas').getContext("2d");

	// Add mouse events
	// ----------------
	$('#canvasSimpleSizes').mousedown(function(e)
			{
		// Mouse down location
		//var mouseX = e.pageX - this.offsetLeft - document.getElementById('container').offsetLeft;
		var mouseX = e.pageX - this.offsetLeft - document.getElementById('container').offsetLeft;
		//var mouseY = e.pageY - this.offsetTop - document.getElementById('container').offsetTop;
		var mouseY = e.pageY - this.offsetTop - document.getElementById('container').offsetTop;

		paint_simpleSizes = true;
		addClickSimpleSizes(mouseX, mouseY, false);
		redrawSimpleSizes();
			});

	$('#canvasSimpleSizes').mousemove(function(e){
		if(paint_simpleSizes){
			addClickSimpleSizes(e.pageX - this.offsetLeft - document.getElementById('container').offsetLeft, e.pageY - this.offsetTop - document.getElementById('container').offsetTop, true);
			redrawSimpleSizes();
		}
	});

	$('#canvasSimpleSizes').mouseup(function(e){
		paint_simpleSizes = false;
		redrawSimpleSizes();
	});

	$('#canvasSimpleSizes').mouseleave(function(e){
		paint_simpleSizes = false;
	});


	$('#chooseSmallSimpleSizes').mousedown(function(e){
		curSize_simpleSizes = "small";
	});
	$('#chooseNormalSimpleSizes').mousedown(function(e){
		curSize_simpleSizes = "normal";
	});
	$('#chooseLargeSimpleSizes').mousedown(function(e){
		curSize_simpleSizes = "large";
	});
	$('#chooseHugeSimpleSizes').mousedown(function(e){
		curSize_simpleSizes = "huge";
	});

	$('#clearCanvasSimpleSizes').mousedown(function(e)
			{
		clickX_simpleSizes = new Array();
		clickY_simpleSizes = new Array();
		clickDrag_simpleSizes = new Array();
		clickColor_simpleSizes = new Array();
		clickSize_simpleSizes = new Array();
		clearCanvas_simpleSizes();
			});
	

}

function addClickSimpleSizes(x, y, dragging)
{
	clickX_simpleSizes.push(x);
	clickY_simpleSizes.push(y);
	clickDrag_simpleSizes.push(dragging);
	clickColor_simpleSizes.push(curColor_simpleSizes);
	clickSize_simpleSizes.push(curSize_simpleSizes);
}

function clearCanvas_simpleSizes()
{
	context_simpleSizes.clearRect(0, 0, canvasWidth, canvasHeight);
}

function updateXY(tratto, x, y){
	dim = tratto;
	clickX_simpleSizes = x;
	clickY_simpleSizes = y;
	for(var i=0; i<x.length; i++){
		clickDrag_simpleSizes.push(true);
	}
	addClickSimpleSizes(x[x.length-1],y[y.length-1], true);
	redrawSimpleSizes();
}

function redrawSimpleSizes()
{
	clearCanvas_simpleSizes();

	var radius;
	context_simpleSizes.lineJoin = "round";
	radius= dim;

	for(var i=0; i < clickX_simpleSizes.length; i++)
	{
		
		context_simpleSizes.beginPath();
		if(clickDrag_simpleSizes[i] && i){
			context_simpleSizes.moveTo(clickX_simpleSizes[i-1], clickY_simpleSizes[i-1]);
		}else{
			context_simpleSizes.moveTo(clickX_simpleSizes[i]-1, clickY_simpleSizes[i]);
		}
		context_simpleSizes.lineTo(clickX_simpleSizes[i], clickY_simpleSizes[i]);
		context_simpleSizes.closePath();
		context_simpleSizes.strokeStyle = colorPurple;
		context_simpleSizes.lineWidth = radius;
		context_simpleSizes.stroke();
	}
}

function aggiornaDim(newDim){
	dim=newDim;
	clickX_simpleSizes = new Array();
	clickY_simpleSizes = new Array();
	clickDrag_simpleSizes = new Array();
	clickColor_simpleSizes = new Array();
	clickSize_simpleSizes = new Array();
	clearCanvas_simpleSizes();
}