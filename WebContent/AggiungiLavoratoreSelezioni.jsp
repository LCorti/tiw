<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="database.DBManager" %>
    <%@ page import="java.util.List" %>
    <%@ page import="beans.Utente" %>

<%

HttpSession s = request.getSession(true);
Utente u = (Utente) s.getAttribute("user");
if (u== null || u.getIdUtente()==0||request.getParameter("idCampagna")==null || !"gestore".equals(u.getRuolo())) {
		response.sendRedirect("login.jsp");
}	
else{
%>
<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" href="stylesheets/w3.css">
		<%
			DBManager db = new DBManager();
			List<Utente> lavoratori= db.getLavoratori();
			int idCampagna=Integer.parseInt(request.getParameter("idCampagna"));
			int n= db.getParNCampagna(idCampagna);
			
			
				
			
		%>
		
		
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Inserisci lavoratori per le Selezioni</title>
	</head>
	<body background="${pageContext.request.contextPath}/bgImage/login.jpg">
	<div class="w3-display-container">
		<header>
			<h1>
				Assegnazione Lavoratori al task di selezione 
				
			</h1>
			<h2>
				Puoi scegliere se aggiungere i lavoratori manualmente oppure lasciare che il nostro algoritmo se ne occupi per te!
			</h2>
		</header>
		<hr>
		<p>
			<h3>Usa Algoritmo</h3>
				L'algoritmo si basa sul numero di immagini assegnate ad ogni alvoratore in modo da garantire la più equa distribuzione del carico di lavoro possibile. E' per tanto raccomandabile (sia chiaro, NON OBBLIGATORIO) scegliere quest'opzione solo nel momento in cui si abbia già inserito le immagini della campagna
				<form action='GeneraLavoratoriSelezioni' >
					<input type="hidden" value="<%=idCampagna %>" name="idCampagna" />
					<input type="submit" value="Genera in Automatico" />
				</form>
			
		</p>
		<hr>
	
		<p>
			<h3>
				Seleziona lavoratori manualmente. ATTENZIONE! Inserire Almeno <%=n%> lavoratori
			</h3>
			<details>
			  <summary>ISTRUZIONI</summary>
			  	<ol type="I">
				  <li>Selezionare uno o più lavoratori nella lista di sinistra</li>
				  <li>Schiacciare il tasto ">>" per assegnarli alla task della tua campagna </li>
				  <li>Se cambi idea seleziona i lavoratori già assegnati e schiaccia "<<" per rimuoverli dalla campagna</li>
				  <li>Controlla di aver Assegnato abbastanza lavoratori </li>
				  <li>Schiacciare sul tasto "Conferma" per confermare e tornare alla tua Home Page</li>
				</ol>
			</details>
			<br>						
			<form action="RegistraLavoratori" method="get" onsubmit="return convalida(list2)">
				<table>
					<tr>
						<th>Lavoratori Disponibili</th>
						<th></th>
						<th></th>
						<th>Lavoratori assegnati alla tua campagna</th>
					</tr>
					<tr>
							<td>
								<select id="firstList" size="10" multiple>
								<%
									for(int i=0;i<lavoratori.size();i++){
								%>
								<option value="<%=lavoratori.get(i).getIdUtente()%>"><%=lavoratori.get(i).getNome()%> <%=lavoratori.get(i).getCognome() %>  </option>
								<% 
									}
								%>	
								</select>
								
								
							</td>
							<td>
								Assegna selezionati
								<br />
								<input type="button" onClick="moveOptions(list1, list2)" value=">>">
							</td>
							<td >
								Rimuovi selezionati
								<br />
								<input type="button" onClick="moveOptions(list2, list1)" align="right" value="<<">
							</td>
							<td>
								
								<select id="secondList"  name="lavoratori" size="10" multiple>
									
								</select>
								
							</td>
						</tr>
				</table>		
				<input type="hidden" value="<%=idCampagna %>" name="idCampagna" />
				<input type="hidden" value="sel" name="tipo" />
				<input type="submit"  value="conferma">
			</form>
		</p>
		</div>
		
	
	
	
	
	
	
		<script>
			var list1 = document.getElementById("firstList");
			var list2 = document.getElementById("secondList");
			var n=<%=n%>;
			
			function moveOptions(fromL, toL) {
				for (i = 0; i < fromL.length; i++)
					if (fromL.options[i].selected === true)
						toL.options[toL.length] = new Option(fromL.options[i].text,fromL.options[i].value);
						
				for (i = (fromL.length - 1); i >= 0; i--)
					if (fromL.options[i].selected === true)
						fromL.options[i] = null;
				
			};
			
			
			function convalida(lavL){
				//alert(lavL.length);
				if(lavL.length<n){
					alert("Il numero dei lavoratori deve essere maggiore o uguale a" + n);
					return false;
				}
				
				for (i = 0; i < lavL.length; i++)
					lavL.options[i].selected =true;
				
				
			}
		</script>
	</body>
	
</html>
<%}%>