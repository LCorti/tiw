<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	  <%@ page import="beans.Utente" %>
	  
    <%@ page import="database.*" %>
    <%@ page import="beans.Campagna" %>
    <%@ page import="java.util.List" %>

    
    
<%-- da aggiustare --%>    


<%

HttpSession s = request.getSession(true);
Utente u = (Utente) s.getAttribute("user");
if (u== null || u.getIdUtente()==0 || !"gestore".equals(u.getRuolo())) {
		response.sendRedirect("login.jsp");
}	
else{
%>
<jsp:useBean id="user" class="beans.Utente"  scope="session"/>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="stylesheets/w3.css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Campagne <jsp:getProperty property="nome" name="user"/></title>
		<script type="text/javascript" src="scriptJS/jQuery.js"></script>
	</head>
		
	<body background="${pageContext.request.contextPath}/bgImage/login.jpg">
		<div class="w3-display-container" style="width:90%; margin:0 auto;">
			<header>
			<h1>Home Page di <jsp:getProperty property="nome" name="user"/> <jsp:getProperty property="cognome" name="user"/></h1>
			<hr>
			<h2>
				Benvenuto sulla tua Home Page! Clicca su una delle tue campagne per completarne la creazione, oppure, se già avviata, per consultarne le statistiche di avanzamento!
			</h2>
			</header>
			<section>
			<% 
			DBManager db = new DBManager();
			List<Campagna> c=db.getCampagnePerGestore(u);
			if (c.size()!=0){%>
			<table style="border:solid;" style="width:80%; margin:auto;">
				<tr style="border:solid;">
					<td style="width:60%; margin:0 auto;">
						<table class="w3-table-all w3-hoverable" style="width:100%;margin-top:0px;">
							<thead>
							<tr class="w3-green">
								<th>#</th>
								<th>Titolo</th>
								<th>Editabile</th>
								<th>Conclusa</th>
							</tr>
							</thead>
							<% 
								
								
										for(int i=0;i<c.size();i++){
											
											%>
											<tr class="content">
												<td style="width:50px" onclick="aggiornaStat(<%= c.get(i).getIdCampagna()%>)"><%= c.get(i).getIdCampagna()%></td>
												<td onclick="aggiornaStat(<%= c.get(i).getIdCampagna()%>)"><%= c.get(i).getTitolo()%></td>
												<td onclick="aggiornaStat(<%= c.get(i).getIdCampagna()%>)"><%= c.get(i).isEditabile()%></td>
												<td onclick="aggiornaStat(<%= c.get(i).getIdCampagna()%>)"><%= c.get(i).isConclusa()%></td>
											</tr>
											<% 
										}
							%>
						</table>
					</td>
					<td style=" width:60%; margin:0 auto;">
						<div style=" border: 1px solid gray; border-radius: 10px; " >
							<div id="stat" style=" padding-top: 20px;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;">
								Clicca su una delle tue campagne per iniziare...
							</div>
						</div>
					</td>
					
				</tr>	
			</table>
			<%}else{ %>
				Crea una nuova campagna per iniziare!
			<%} %>
			</section>
			<aside>
			
			<br><br><br>
			<form action="NuovaCampagna.jsp">
	    		<input type="submit" value="NUOVA CAMPAGNA" />
			</form>
			<div class="w3-display-topright" style="right:10px;"><form action="LogoutServlet">
	    		<input type="submit" value="Logout" />
			</form></div>
		</div>
		
		<script type="text/javascript">
			function aggiornaStat(idC){
				
				
				$.ajax({
		            url:'InfoCampagna',
		            data:{idCampagna : idC},
		            type:'get',
		            cache:false,
		            success:function(data){
		               //alert(data);
		               $('#stat').html(data); 
		            },
		            error:function(){
		              alert('error');
		            }
		         }
				);		
			}
		
		</script>
		</aside>
	</body>
</html>
<%}%>